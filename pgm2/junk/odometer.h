/******************************************************************************

Class Name: Odometer
Data:	    miles_driven - tracks the miles driven
	    fuel_efficiency - tracks fueld efficiency in MPG
	    fuel - is used to track fuel (in gallons)
	    mileage - is used to track mileage for an automobile
	    
Mutator Functions:
    Set_Odometer_Reset - resets the Odometer to zero miles
    Set_Fuel_Effic - sets fuel efficiency
    Set_Odometer_Total - takes miles driven for a trip and + to Odometer total

Accessor Functions:
    Get_Gasoline - returns gallons/gasoline that has been consumed since reset
    

Functions:



*/

#include "odometer.h"

class Odometer
{
  float gallons; //
  //float miles_driven; //tracks miles driven
  float fuel_efficiency, mileage; //tracks fuel efficiency in MPG and mileage
  //float odometer_total;
  
  //calculation
  void gas_used();
  
  Public:
  //Mutator functions
  void add_mileage(float);
  void set_fuel_effic(float);
  void reset_mileage();
  //void Set_Odometer_Total();
  
  //Accessor functions
  //void Get_Gasoline_Consumed;
  float get_gallons();
  float get_mileage();
  float get_fuel_effic();
  
  //Calculations
  //float odometer_calculate();
  
  
};

// set miles / get miles
// mpg = miles/gallons - fuel_efficiency
//gallons
// gallons = miles/mpg(fuel_efficiency) CALCULATED
// tracking-USER INPUT-fuel_efficiency and miles (Driven)
//miles reset or add to it
//fuel_efficiency can add to it