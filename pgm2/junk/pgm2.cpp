/******************************************************************************
main function:
   We allow for the user to input mileage and fuel efficiency
      The class is then called to make calculations for gallons, ect ect



//0out mileage, set mileage, set mpg, calc gallons-x3
*/

#include "odometer.cpp"
#include <iostream>
#include <string>

using namespace std;

int main(void)
{
   Odometer vehicle;
   float miles_driven;
   float MPG;
   
   cout << endl;
   cout << "I will calculate your gas used during three different trips"
   << endl << endl;
   
   for(int trip_number = 1; trip_number < 4; trip_number++)
   {
   vehicle.reset_mileage();
   cout << endl;
   cout << "For trip number: " << trip_number << endl;
   cout << "Enter the miles driven: ";
   cin >> miles_driven;
   cout << "Enter the Fuel Efficiency (MPG): ";
   cin >> MPG;
   
   vehicle.add_mileage(miles_driven);
   vehicle.set_fuel_effic(MPG);
   
   cout << "You have used " << vehicle.gas_used() << " gallons of gasoline"
   << " during trip number: " << trip_number << endl;
   }
   
   cout << endl;
   
   return 0;
}