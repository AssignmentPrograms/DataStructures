/******************************************************************************
   Brandon Jones
   N534H699

       Program #2
   
   Description of the problem
        This program implements the Odometer class. It contains gallons, fuel
      fuel efficiency, and mileage. The user gets to choose the miles driven
      (mileage) and the fuel efficiency (MPG) of the vehicle.
      In the program, we use a loop to show three possible trip variations, at
      the begining of each loop we 0 out the mileage, then allow the user to
      set the mileage and add to it within a while loop, to build up the total
      miles driven for that trip. Then the user gets to set the MPG, and then
      the calculation of gallons used is determined and displayed to show the
      gallons used for that trip.



//0out mileage, set mileage, set mpg, calc gallons-x3
*/

#include "odometer.cpp"
#include <iostream>
#include <string>
using namespace std;

int main(void)
{
   //vehicle is of type Odometer, the class we used in our other files
   Odometer vehicle;
   
   //miles_driven is to store the miles driven during a given trip
   float miles_driven;
   
   //MPG is the fuel efficiency of the vehicle the user enters along
   //with miles_driven to be used to calculate gallons used during a trip
   float MPG;
   
   cout << endl;
   cout << "I will calculate your gas used during three different trips"
   << endl << endl;
   
   int trip_number;
   cout << "How many trips are you calculating for? ";
   cin >> trip_number;
   
      for(int track = 1; track <= trip_number; track++)
      {
      vehicle.reset_mileage();
      cout << endl;
      cout << "For trip number: " << track << endl;
   
         char answer;
         do
         {
         cout << "Enter the miles driven: ";
         cin >> miles_driven;
         //vehicle.add_mileage(miles_driven);
         cout << "You have so far drove: " << vehicle.add_mileage(miles_driven)
         << " miles" << endl;
	    bool valid = 0;
	    do
	    {
            cout << "Do you wish to add more miles driven for trip: "
            << track << "? ('y'/'n')" << endl;
            cin >> answer;
            answer = toupper(answer);
	       if(answer == 'N' || answer == 'Y')
	       {
		  valid = 1;
	       }
	    }while(valid != 1);   
         }while(answer != 'N');
   
      cout << "Enter the Fuel Efficiency (MPG): ";
      cin >> MPG;
      vehicle.set_fuel_effic(MPG);
   
      cout << "You have used " << vehicle.gas_used() << " gallons of gasoline"
      << " during trip number: " << track << endl;
      }
   
   cout << endl;
   
   return 0;
}