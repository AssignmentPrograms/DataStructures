/******************************************************************************
   
   Brandon Jones
   N534H699

       Program #2
   
   Description of the problem
        This program implements the Odometer class. It contains gallons, fuel
      fuel efficiency, and mileage. The user gets to choose the miles driven
      (mileage) and the fuel efficiency (MPG) of the vehicle.
      In the program, we use a loop to show three possible trip variations, at
      the begining of each loop we 0 out the mileage, then allow the user to
      set the mileage and add to it within a while loop, to build up the total
      miles driven for that trip. Then the user gets to set the MPG, and then
      the calculation of gallons used is determined and displayed to show the
      gallons used for that trip.


   Psuedo Code
   THIS WAS DONE BEFORE ATTEMPTING ASSIGNMENT- NOT 100% LIKE FINAL PROGRAM
    Class Name: Odometer
    Data:	  miles_driven - tracks the miles driven
	          fuel_efficiency - tracks fueld efficiency in MPG
	          fuel - is used to track fuel (in gallons)
	          mileage - is used to track mileage for an automobile
	     
    Mutator Functions:
                 Set_Odometer_Reset - resets the Odometer to zero miles
                 Set_Fuel_Effic - sets fuel efficiency
                 Set_Odometer_Total - takes miles driven for a trip 
                 and + to Odometer total
 
    Accessor Functions:
                Get_Gasoline - returns gallons/gasoline that has been consumed
                since reset
    

    Functions:
                Calc - This function calculates the gallons used during a trip
                       it uses mileage and fuel_efficiency to get MPG



*/



#ifndef ODOMETER_H
#define ODOMETER_H


class Odometer
{
  float gallons; //
  //float miles_driven; //tracks miles driven
  float fuel_efficiency, mileage; //tracks fuel efficiency in MPG and mileage
  //float odometer_total;
  
  
  
  public:
  //Mutator functions
  float add_mileage(float);
  void set_fuel_effic(float);
  void reset_mileage();
  //void Set_Odometer_Total();
  
  //Accessor functions
  //prototypes the get_gallons function
  float get_gallons();
  
  //DELETED: float get_mileage();
  //DELTED: float get_fuel_effic();
  //DELTED: void Get_Gasoline_Consumed;
  
  //calculation
  //prototypes the gas_used function
  float gas_used();
  
  //DELTED: float odometer_calculate();
  
  
};

// set miles / get miles
// mpg = miles/gallons - fuel_efficiency
//gallons
// gallons = miles/mpg(fuel_efficiency) CALCULATED
// tracking-USER INPUT-fuel_efficiency and miles (Driven)
//miles reset or add to it
//fuel_efficiency can add to it

#endif