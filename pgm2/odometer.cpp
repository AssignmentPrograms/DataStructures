/******************************************************************************
//Function Definitions

   Brandon Jones
   N534H699

       Program #2
*/



#include <iostream>
#include <string>
#include "odometer.hpp"

using namespace std;

//Mutator Functions
void Odometer::reset_mileage()
{
   mileage = 0;
}

float Odometer::add_mileage(float mil)
{
  mileage += mil;
  return mileage;
}

//User inputs fuel_eff
void Odometer::set_fuel_effic(float fuel_eff)
{
   fuel_efficiency = fuel_eff;
   gas_used();
}


//Calculates gallons used (MILES/MPG)
float Odometer::gas_used()
{
  gallons = mileage/fuel_efficiency;
  return gallons;
}



/*
 * DELTED:
float Odometer::get_mileage()
{
  return mileage;
}

float Odometer::get_fuel_effic()
{
  return fuel_efficiency;
  
}
*/



/*
 * DELTED:
float Odometer::odometer_calculate()
{
  odometer_total += miles_driven;
}
*/

//Accessor
float Odometer::get_gallons()
{
  gas_used();
  return gallons;
}
