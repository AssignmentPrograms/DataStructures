/****************************************************************************** 
   Brandon Jones
   N534H699

       Program #3
   
  Description of problem:
     The user creates two fractions if needed, and then has the option to add, 
     subtract, multiply and divide. The user may also reduce the fraction.
     The user chooses numerator and denominator for both fractions and then
     can choose the operations to do on them or if the program is to simplify
     or get decimal equilivant for the given fraction.
      
  Psudo Code
     Class Name: Fraction
     Data:       numerator1 - holds the numerator of a fraction
		 denominator1 - holds the denominator of a fraction
     Accessors Functions:
                 get_numerator1 - returns numerator1
                 get_denominator1 - returns denominator1
     Functions:
                 Frac - finds the decimal equilavent
                 Reduce_Numerator - reduces numerator
                 Reduce_Denominator- reduces denominator
                 operator+ - allows for fractional addition
                 operator- - allows for fractional subtraction
                 operator* - allows for fractional multiplication
                 operator/ - allows for fractional division


*/

#ifndef FRACTION_H
#define FRACTION_H


class Fraction
{
   int numerator1; //private top part of fraction
   int denominator1; //private bottom part of fraction
   
   
   
public:
   //Constructor
   Fraction(int num1, int num2);
   
   
   
   //Default Constructor
   Fraction();
   
   //Mutators
   
   //Accessors
   int get_numerator1();
   int get_denominator1();
   
   //Member Functions
   double Frac(int, double);
   int Reduce_Numerator(int, int);
   int Reduce_Denominator(int, int);
   //void Fraction_Form(/*const Fraction &f3*/);
   //void print_fraction(const Fraction &f3);
  
   //Operator Functions
   Fraction operator+(const Fraction &f2);
   Fraction operator-(const Fraction &f2);
   Fraction operator*(const Fraction &f2);
   Fraction operator/(const Fraction &f2);
  
  
  
};

#endif