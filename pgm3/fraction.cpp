/******************************************************************************
/*
 
   Brandon Jones
   N534H699

       Program #3
   
*/

#include <iostream>
#include "fraction.hpp"
using namespace std;


//Constructor
Fraction::Fraction(int num1, int num2)
{
   numerator1 = num1;
   denominator1 = num2;
  
}

//Default Constructor
Fraction::Fraction()
{
   numerator1 = 1;
   denominator1 = 1;
   
}

//Accessor function to give access to private variable
int Fraction::get_numerator1()
{
   return numerator1;
}

//Accessor function to give access to private variable
int Fraction::get_denominator1()
{
   return denominator1;
}

//returns the decimal equilavent of a given fraction
double Fraction::Frac(int numerator1, double denominator1)
{
   return numerator1/denominator1;
  
}

//finds the Greatest common divisor to be used in the reduce_ functions
double GCD(int numerator1, int denominator1)
{
   if(denominator1 == 0)
   {
     return numerator1;
   }
     return GCD(denominator1, numerator1%denominator1);
}

//reduces the numerator to the GCD
int Fraction::Reduce_Numerator(int numerator1, int denominator1)
{
   int divisor;
   divisor = GCD(numerator1, denominator1);
   numerator1 = numerator1 / divisor;
   return numerator1;
   //denominator1 = denominator1 / divisor;
   //cout << numerator1 << "/" << denominator1;
}

//reduces the denominator to the GCD
int Fraction::Reduce_Denominator(int numerator1, int denominator1)
{
   int divisor;
   divisor = GCD(numerator1, denominator1);
   denominator1 = denominator1 / divisor;
   return denominator1;
}

/*
void Fraction::Fraction_Form(const Fraction &f3)
{
   cout << numerator1 << "/" << denominator1;
}
*/

/*
void Fraction::print_fraction(const Fraction &f3)
{
   cout << numerator1 << "/" << denominator1;
}
*/

//Overloads the + operator to be able to add fractions accurately
Fraction Fraction::operator+(const Fraction &f2)
{
   int new_numerator1;
   new_numerator1 = numerator1 * f2.denominator1;
   int new_numerator2;
   new_numerator2 = f2.numerator1 * denominator1;
   int new_denominator;
   new_denominator = denominator1 * f2.denominator1;
   int numerator_sum;
   numerator_sum = new_numerator1 + new_numerator2;
   //cout << numerator_sum << "/" << new_denominator;
   return Fraction(numerator_sum, new_denominator);
}

//Overloads the - operator to be able to subtract fractins accurately
Fraction Fraction::operator-(const Fraction &f2)
{
   int new_numerator1;
   new_numerator1 = numerator1 * f2.denominator1;
   int new_numerator2;
   new_numerator2 = f2.numerator1 * denominator1;
   int new_denominator;
   new_denominator = denominator1 * f2.denominator1;
   int numerator_difference;
   numerator_difference = new_numerator1 - new_numerator2;
   //cout << numerator_sum << "/" << new_denominator;
   return Fraction(numerator_difference, new_denominator);
}

//Overloads the * operator to be able to multiply fractions accurately
Fraction Fraction::operator*(const Fraction &f2)
{
   int new_numerator;
   new_numerator = numerator1 * f2.numerator1;
   int new_denominator;
   new_denominator = denominator1 * f2.denominator1;
   //cout << new_numerator << "/" << new_denominator;
   return Fraction(new_numerator, new_denominator);
}

//Overloads the / operator to be able to divide fractins accurately
Fraction Fraction::operator/(const Fraction &f2)
{
   int new_numerator;
   new_numerator = numerator1 * f2.denominator1;
   int new_denominator;
   new_denominator = denominator1 * f2.numerator1;
   //cout << new_numerator << "/" << new_denominator;
   return Fraction(new_numerator, new_denominator);
}