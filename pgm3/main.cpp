/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #3
   
   Description of problem:
      The user creates two fractions if needed, and then has the option to add, 
      subtract, multiply and divide. The user may also reduce the fraction.
      The user chooses numerator and denominator for both fractions and then
      can choose the operations to do on them or if the program is to simplify
      or get decimal equilivant for the given fraction.
   


*/

#include <iostream>
#include "fraction.cpp"
using namespace std;

int main(void)
{
   int selection; //selection is to record user input for a given menu
   //Fraction 1
   int numerator1; // this is the top part of fraction 1
   int denominator1; // this is the bottom part of fraction 1
   //Fraction 2
   int numerator2; // this is the top part of fraction 2
   int denominator2; // this is the bottom part of fraction 2
   //Fraction 3
   int numerator3; // this is the top part of fraction 3
   int denominator3; // this is the bottom part of fraction 3
   
   int newnum; // this is to store the reduced numerator
   int newden; // this is to store the reduced denominator
   char choice; // this is to store the users input for a given menu
   char answer; // this is to store the users input for a given question
   
   // this loop is to provide the user repeatability before program end
   do
   {
      // this is to allow the user to test the default constructor
      cout << "Do you wish to test the default constructor? (y/n): ";
      cin >> answer;
      answer = toupper(answer);
      
   
      // this is to allow the user to make a selection for what to do to
      // the default constructed fraction
      if(answer == 'Y')
      {
         Fraction f1;
         Fraction f2;
         Fraction f3;
      
         cout << "Make a selection below:" << endl;
         cout << "1. Add" << endl;
         cout << "2. Subtract" << endl;
         cout << "3. Multiply" << endl;
         cout << "4. Divide" << endl;
         cout << "5. Get Decimal Equivalent" << endl;
         cout << "6. Reduce Fraction" << endl;
         cin >> selection;
	 cout << endl;
   
	 //interaction with the menu
         switch(selection)
         {
           case 1:
	      f3 = f1 + f2;
	      cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	      cout << endl;
              break;    
           case 2:
              f3 = f1 - f2;
	      cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	      cout << endl;
              break;    
           case 3:
              f3 = f1 * f2;
	      cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	      cout << endl;
              break;
           case 4:
              f3 = f1 / f2;
	      cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	      cout << endl;
              break;
           case 5:
	      cout << "1" << endl;
	      cout << endl;
	      break;
           case 6:
	      cout << f1.get_numerator1() << "/" << f1.get_denominator1() << endl;
	      cout << endl;
	      break;
         }   
      }
   
      // if the user does not wish to use the default constructor this code runs
      if(answer == 'N')
      {
	 // creates the fraction f3 to be used to store an answer(sum, ect) later
         Fraction f3;
	 
	 //the user now enters the custom fraction to be used later
         cout << "Enter in a numerator for a fraction" << endl;
         cin >> numerator1;
         cout << "Enter in a denominator for a fraction" << endl;
         cin >> denominator1;
	 //stores the input into fraction object f1
         Fraction f1(numerator1, denominator1);
      
	 //asks if the user wishes to do math operations, stores the answer
	 //then makes a decision on which code to run
         cout << "Do you wish to do math operations on two fractions? (y/n): ";
         cin >> answer;
         answer = toupper(answer);
	 
	 //if the user wishes to do math operations we then output
	 //the given math operator options to use on the fraction and a
	 //second fraction to do the operations with, which the user enters
         if(answer == 'Y')
            {
               cout << "Enter the second fraction" << endl;
               cout << "Note: This will be the divisor/subtrahend" << endl;
               cout << "Enter the numerator: ";
               cin >> numerator2;
               cout << "Enter the denominator: ";
               cin >> denominator2;
      
               Fraction f2(numerator2, denominator2);
      
               cout << "Make a selection below:" << endl;
               cout << "1. Add" << endl;
               cout << "2. Subtract" << endl;
               cout << "3. Multiply" << endl;
               cout << "4. Divide" << endl;
      
               cin >> selection;
      
	       //interacts with menu and runs code based off user input
               switch(selection)
               {
		  //adds two fractions using the overloader + function
	          case 1:
	             f3 = f1 + f2;
	             cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	             cout << endl;
                     break;   
		  //subtracts two fractions using the overloader - function   
                  case 2:
                     f3 = f1 - f2;
	             cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	             cout << endl;
                     break; 
		 //multiplies two fractions using the overloader * function    
                 case 3:
                    f3 = f1 * f2;
	            cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	            cout << endl;
                    break;
		 //divides two fractions using the overloader / function
                 case 4:
                    f3 = f1 / f2;
	            cout << f3.get_numerator1() << "/" << f3.get_denominator1() << endl;
	            cout << endl;
                    break;
                }
      
            }
      
      
	    //if the user decides to not use math operations we then present
	    // the remaining options for the user
            if(answer == 'N')
            {
               cout << "Make a selection: " << endl;
      
               cout << "1. Get Decimal Equivalent" << endl;
               cout << "2. Reduce Fraction" << endl;
               cin >> selection;
	     
	       // runs appropriate code to do as the user commanded
	       switch(selection)
	       {
	          case 1:
	             cout << f1.Frac(numerator1, denominator1) << endl;
	             break;
	          case 2:
	             newnum = f1.Reduce_Numerator(numerator1, denominator1);
	             newden = f1.Reduce_Denominator(numerator1, denominator1);
	             cout << newnum << "/" << newden << endl;
	             break;
	       }  
            }
      }
      
      //gives the user the option to repeat the code
      cout << "Do you wish to do another? (y/n): ";
      cin >> choice;
      choice = toupper(choice);
      
   //loop ends at users request
   }while(choice != 'N');
}