//Function Definitions

/******************************************************************************

 Functions:
 
 Set_Fuel_Effic - 
    Pre-Conditions -
    Post-Conditions -
 
 Set_Odometer_Reset -
 
 Set_Odometer_Total -
 
 Get_Gasoline_Consumed -
 
 
*/



//User inputs mil

float Odometer::get_gallons()
{
  return gallons;
}

float Odometer::get_mileage()
{
  return mileage;
}

float Odometer::get_fuel_effic()
{
  return fuel_efficiency;
}



void Odometer::reset_mileage()
{
   mileage = 0;
   gas_used();
}

void Odometer::add_mileage(float mil)
{
  mileage += mil;
  gas_used();
}

//User inputs fuel_eff
void Odometer::set_fuel_effic(float fuel_eff)
{
   fuel_efficiency = fuel_eff;
   gas_used();
}



void Odometer::gas_used();
{
  gallons = mileage/fuel_efficiency;
}
/*
float Odometer::odometer_calculate()
{
  odometer_total += miles_driven;
}
*/
  