/******************************************************************************

   Brandon Jones
   N534H699

       Program #8
   
   Description of problem:
      Customer wishes to create a contact list of phone numbers and names
      
   Classes:
   
   BST
      private:
         AddLeaf Private: Adds a leaf to a node in the correct spot
         PrintinOrderPrivate: Prints the tree in numerical order
         ReturnNodePrivate: Returns a wanted node
         ReturnNumberPrivate: Returns the contect of the node returned in
         ReturnNode
      
         FindSmallestPrivate: Finds smallest leaf in a wanted subtree
         RemoveNodePrivate: Finds a node to remove
         RemoveMatch: Removes a match found in our remove node functions
         RemoveRootMatch: Removes the root of the tree successfully
         RemoveSubTree: Removes a portion of the tree as willed
      public:
         CreateLeaf: creates a leaf on the tree
         PrintChildren: Prints the children of the nodes
         ReturnRootKey: Returns the key of the current rootKey
      
         ALL REST OF PUBLIC FUNCTIONS ARE BUILDER FUNCTIONS FOR
	    PRIVATE FUNCTIONS IN ORDER TO KEEP NEEDED DATA PRIVATE



*/


#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
using namespace std;

//BST IS THE CLASS THAT DOES ALL BINARY SEARCH TREE FUNCTIONS AND ACTIONS
class BST
{
    //Node to hold the information of what a ndoe is
    struct Node
    {
        unsigned long long int key;
        string name;
        Node* left;
        Node* right;
    };
    Node* root;
    //adds leaf to a tree
    void AddLeafPrivate(unsigned long long int key, Node* ptr, string name)
    {
        if(root == NULL)
        {
            root = CreateLeaf(key, name);
        }
        else if(key < ptr->key)
        {
            if(ptr->left != NULL)
            {
                AddLeafPrivate(key, ptr->left, name);
            }
            else
            {
                ptr->left = CreateLeaf(key, name);
            }
        }
        else if(key > ptr->key)
        {
            if(ptr->right != NULL)
            {
                AddLeafPrivate(key, ptr->right, name);
            }
            else
            {
                ptr->right = CreateLeaf(key, name);
            }
        }
        else
        {
            cout << "The key " << key << " has already been added to the tree\n";
        }
    }
    //Prints in order to the screen
    void PrintInOrderPrivate(Node* ptr)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderPrivate(ptr->left);
            }
            cout << "Number: " << ptr->key << " " << "Name: " << ptr->name << endl;;
            if(ptr->right != NULL)
            {
                PrintInOrderPrivate(ptr->right);
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    //prints a preorder format to a file
    void PrintPreOrderPrivate(Node* ptr, ofstream& OutputFile, int level)
    {
       for(int i = 1; i <= level; i++)
       {
	  OutputFile << "     ";
       }
      
       if(root != NULL)
       {
	  OutputFile << "Number: " << ptr->key << "     " << "Name: " << ptr->name << endl;
	  
	  if(ptr->left !=NULL)
	  {
	     PrintPreOrderPrivate(ptr->left, OutputFile, level + 1);
	  }
	  if(ptr->right != NULL)
	  {
	     PrintPreOrderPrivate(ptr->right, OutputFile, level + 1);
	  }
       }
       else
       {
          OutputFile << "The tree is empty" << endl;
       }
    }
    //returns a desired node
    Node* ReturnNodePrivate(unsigned long long int key, Node* ptr)
    {
        
        if(ptr != NULL)
        {
            if(ptr->key == key)
            {
                return ptr;
            }
            else
            {
                if(key < ptr->key)
                {
                    return ReturnNodePrivate(key, ptr->left);
                }
                else//Greater than
                {
                    return ReturnNodePrivate(key, ptr->right);
                }
            }
        }
        else
        {
            return NULL;
        }
    }
    //returns a number desired
    void ReturnNumberPrivate(unsigned long long int key)
    {
        Node* ptr = new Node;
        int number;
	string name;
	ptr = ReturnNode(key);
	
        if(ptr != NULL)
        {
            cout << "Number: " << ptr->key << endl;
	    cout << "Name: " << ptr->name << endl;
        }
        else
        {
            cout << "The contact number is not in the list" << endl;
        }
    }
    //Finds smallest node ina  given subtree
    unsigned long long int FindSmallestPrivate(Node* ptr)
    {
        if(root == NULL)
        {
            cout << "The tree is empty\n";
            return -1000; // needs to adjust if there are negative numbers
        }
        else
        {
            if(ptr->left != NULL)
            {
                return FindSmallestPrivate(ptr->left);
            }
            //no need to look at right child (would always be bigger than
            else
            {
                return ptr->key;
            }
        }
    }
    //Removes a node desired to be deleted
    void RemoveNodePrivate(int key, Node* parent)
    {
        if(root != NULL)
        {
            if(root->key == key)
            {
                RemoveRootMatch();
            }
            else
            {
                if(key < parent->key && parent->left != NULL)
                {
                    if(parent->left->key == key)
		    {
                    RemoveMatch(parent, parent->left, true);
		    }
		    else
		    {
                    RemoveNodePrivate(key, parent->left);
		    }
                }
                else if(key > parent->key && parent->right != NULL)
                {
                    if(parent->right->key == key)
		    {
                    RemoveMatch(parent, parent->right, true);
		    }
		    else
		    {
                    RemoveNodePrivate(key, parent->right);
		    }
                }
                else
                {
                    cout << "The key " << key << " was not found in the tree\n";
                }
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    //Removes root and deltes tree
    void RemoveRootMatch()
    {
        if(root != NULL)
        {
	    Node* delPtr = new Node; 
            delPtr->key = root->key;
            int rootKey = root->key;
            int smallestInRightSubtree;

            // Case 0 - 0 children
            if(root->left == NULL && root->right == NULL)
            {
                root = NULL;
                delete delPtr;
            }

            // Case 1 - 1 child
            else if(root->left == NULL && root->right != NULL)
            {
                root = root->right;
                delPtr->right = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }
            else if(root->left != NULL && root->right == NULL)
            {
                root = root->left;
                delPtr->left = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }

            // Case 2 - 2 children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(root->right);
                RemoveNodePrivate(smallestInRightSubtree, root);
                root->key = smallestInRightSubtree;
                cout << "The root key containing key " << rootKey <<
                        " was overwritten with key " << root->key << endl;
            }
        }
        else
        {
            cout << "Can not remove root. The tree is empty\n";
        }
    }
    //finds where anode is we wish to delete and adjust pointers as needed
    //deletes the unwanted node
    void RemoveMatch(Node* parent, Node* match, bool left)
    {
        if(root != NULL)
        {
            Node* delPtr = new Node;
            int matchKey = match->key;
            int smallestInRightSubtree;

            // Case 0 - 0 Children
            if(match->left == NULL && match->right == NULL)
            {
                delPtr->key = match->key;
		if(parent->left->key == match-> key)
		{
		   parent->left = NULL;
		}
		else
		{
		   parent->right = NULL;
		}
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 1 - 1 Child
            else if(match->left == NULL && match->right != NULL)
            {
	        if(parent->left->key == match->key)
		{
		   parent->left = match->right;
		}
		else
		{
		   parent->right = match->right;
		}
                match->right = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }
            else if(match->left != NULL && match->right == NULL)
            {
	        if(parent->left->key == match->key)
		{
		   parent->left = match->left;
		}
		else
		{
		   parent->right = match->left;
		}
                parent->left = match->left;
                match->left = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 2 - 2 Children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(match->right);
                RemoveNodePrivate(smallestInRightSubtree, match);
                match->key = smallestInRightSubtree;
		cout << smallestInRightSubtree << endl;
            }
        }
        else
        {
            cout << "Can not remove match. The tree is empty\n";
        }
    }
    //removes the subtree of a given parent
    void RemoveSubtree(Node* ptr) //post order traversal actually
    {
        if(ptr != NULL)
        {
            if(ptr->left != NULL)
            {
                RemoveSubtree(ptr->left);
            }
            if(ptr->right != NULL)
            {
                RemoveSubtree(ptr->right);
            }
            cout << "Deleting the node containing key " << ptr->key << endl;
            delete ptr;
        }
    }

public:
    //default constructor for a created BST object
    BST()
    {
        root = NULL;
    }
    //deconstructor of BST
    ~BST()
    {
        RemoveSubtree(root);
    }
    //creates aleaf
    Node* CreateLeaf(unsigned long long int key, string name)
    {
        Node* node_ptr = new Node;
        node_ptr->key = key; // first key is the key inside node, second key is the key being passed in
        node_ptr->name = name;
        node_ptr->left = NULL;
        node_ptr->right = NULL;

        return node_ptr;
    }
    //calls parent function
    void AddLeaf(unsigned long long int key, string name)
    {
        AddLeafPrivate(key, root, name);
    }
    //calls parent function
    void PrintInOrder()
    {
        PrintInOrderPrivate(root);
    }
    //calls parent function
    Node* ReturnNode(unsigned long long int key) //helper function //add to private string name
    {
        ReturnNodePrivate(key, root);
    }
    //calls parent function
    void ReturnNumber(unsigned long long int key) //helper function //add to private string name
    {
        ReturnNumberPrivate(key);
    }
    unsigned long long int ReturnRootKey()
    {
        if(root != NULL)
        {
            return root->key;
        }
        else
        {
            //will need to adjust if we have negative numbers
        }
    }
    //calls parent function
    void PrintChildren(unsigned long long int key)
    {
        Node* ptr = ReturnNode(key);

        if(ptr != NULL)
        {
            cout << "Parent Node = " << ptr->key << endl;
            //cout << "NAME: " << ptr->name << endl;

            ptr->left == NULL ?
            cout << "Left Child = NULL\n":  //true
            cout << "Left Child = " << ptr->left->key << endl; //false
            //cout << "NAME: " << ptr->name << endl;

            ptr->right == NULL ?
            cout << "Right Child = NULL\n":
            cout << "Right Child = " << ptr->right->key << endl;
            //cout << "NAME: " << ptr->name << endl;
        }
        else
        {
            cout << "Key " << key << " is not in the tree\n";
        }
    }
    //calls parent function
    unsigned long long int FindSmallest()
    {
        FindSmallestPrivate(root);
    }
    //calls parent function 
    void RemoveNode(unsigned long long int key)
    {
        RemoveNodePrivate(key, root);
    }
    //calls parent function
    void PrintPreOrder(ofstream& OutputFile)
    {
        PrintPreOrderPrivate(root, OutputFile, 0);
    }


};


int main()
{
    BST tree;
    //stores the choice for our menu
    int choice;
    //stores user entered phone number
    unsigned long long int number;
    //stores user entered name
    string name;

    cout << "Printing the tree in order\nBefore adding numbers\n";

    //print tree with no nodes
    tree.PrintInOrder();

    cout << endl;
      
    //do/while loop to provide repeatability within our menu
    do
    {
    
    cout << "Choose an option from the following (only '1', '2', '3', '4' or '5':\n";
    cout << "1. Add a new telephone number and name into the tree\n";
    cout << "2. Delete a number from the tree\n";
    cout << "3. Locate a telephone number in the tree (if it exists)\n";
    cout << "4. Print the tree\n";
    cout << "5. Quit the program\n";
    cin >> choice;
      
    
    //switch to provide a menu to the user
    switch(choice)
    {
       //user wishes to add new telephone number and name
       case 1:
          cout << "Enter telephone number (no spaces or dash, or special characters) and name into the tree: \n";
          cin >> number;
          cout << "Enter the name: ";
	  cin.ignore(1000,'\n');
          getline(cin, name);
          tree.AddLeaf(number, name);
          break;
       //user wishes to delete a number from the tree  
       case 2:
	  cout << endl;
          cout << "Enter number to delete (if it exists): ";
          cin >> number;
	  tree.RemoveNode(number);
	  cout << endl;
          break;
       //user wishes to locate a telephone number in the tree  
       case 3:
          cout << "Locate a telephone number in the tree: ";
          cin >> number;
	  cout << endl;
	  cout << "Searching..." << endl;
	  tree.ReturnNumber(number);
	  cout << endl;
          break;
       //user wishes to print the tree  
       case 4:
	  cout << endl;
          cout << "The Tree containing your names and numbers is the following\n";
          tree.PrintInOrder();
          cout << endl;
          break;
    }
    //BREAK
    }while(choice != 5);
    //current tree gets printed in preorder to the file tree.dat
    cout << "The current tree will be printed to a file called tree.dat" << endl;
    
    
    //opens a file and checks success status
    ofstream OutputFile("tree.dat");
    if(OutputFile.fail())
    {
       cout << "Failed to open" << endl;
       exit(EXIT_FAILURE);
    }
    
    //prints to file
    tree.PrintPreOrder(OutputFile);
    
    //closes file
    OutputFile.close();
    return 0;
}
