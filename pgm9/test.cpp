#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

/*
class Node
{
    unsigned long long int data;
    Node* left;
    Node* right;
public:
    node()
    {
        data = 0;
        left = NULL;
        right = NULL;
    }
    node(key)
    {
        data = key;
        left = NULL;
        right = NULL;
    }


};
*/

class BST
{
    struct Node
    {
        unsigned long long int key;
        string name;
        Node* left;
        Node* right;
    };
    Node* root;
    void AddLeafPrivate(unsigned long long int key, Node* ptr, string name)
    {
        if(root == NULL)
        {
            root = CreateLeaf(key, name);
        }
        else if(key < ptr->key)
        {
            if(ptr->left != NULL)
            {
                AddLeafPrivate(key, ptr->left, name);
            }
            else
            {
                ptr->left = CreateLeaf(key, name);
            }
        }
        else if(key > ptr->key)
        {
            if(ptr->right != NULL)
            {
                AddLeafPrivate(key, ptr->right, name);
            }
            else
            {
                ptr->right = CreateLeaf(key, name);
            }
        }
        else
        {
            cout << "The key " << key << " has already been added to the tree\n";
        }
    }
    void PrintInOrderPrivate(Node* ptr)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderPrivate(ptr->left);
            }
            cout << "Number: " << ptr->key << " " << "Name: " << ptr->name << endl;;
            if(ptr->right != NULL)
            {
                PrintInOrderPrivate(ptr->right);
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    Node* ReturnNodePrivate(unsigned long long int key, Node* ptr)
    {
        if(ptr != NULL)
        {
            if(ptr->key == key)
            {
                return ptr;
            }
            else
            {
                if(key < ptr->key)
                {
                    return ReturnNodePrivate(key, ptr->left);
                }
                else//Greater than
                {
                    return ReturnNodePrivate(key, ptr->right);
                }
            }
        }
        else
        {
            return NULL;
        }
    }
    unsigned long long int FindSmallestPrivate(Node* ptr)
    {
        if(root == NULL)
        {
            cout << "The tree is empty\n";
            return -1000; // needs to adjust if there are negative numbers
        }
        else
        {
            if(ptr->left != NULL)
            {
                return FindSmallestPrivate(ptr->left);
            }
            //no need to look at right child (would always be bigger than
            else
            {
                return ptr->key;
            }
        }
    }
    
    void RemoveNodePrivate(int key, Node* parent)
    {
        if(root != NULL)
        {
            if(root->key == key)
            {
                RemoveRootMatch();
            }
            else
            {
                if(key < parent->key && parent->left != NULL)
                {
                    if(parent->left->key == key)
		    {
                    RemoveMatch(parent, parent->left, true);
		    }
		    else
		    {
                    RemoveNodePrivate(key, parent->left);
		    }
                }
                else if(key > parent->key && parent->right != NULL)
                {
                    if(parent->right->key == key)
		    {
                    RemoveMatch(parent, parent->right, true);
		    }
		    else
		    {
                    RemoveNodePrivate(key, parent->right);
		    }
                }
                else
                {
                    cout << "The key " << key << " was not found in the tree\n";
                }
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    void RemoveRootMatch()
    {
        if(root != NULL)
        {
	    Node* delPtr = new Node; 
            delPtr->key = root->key;
            int rootKey = root->key;
            int smallestInRightSubtree;

            // Case 0 - 0 children
            if(root->left == NULL && root->right == NULL)
            {
                root = NULL;
                delete delPtr;
            }

            // Case 1 - 1 child
            else if(root->left == NULL && root->right != NULL)
            {
                root = root->right;
                delPtr->right = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }
            else if(root->left != NULL && root->right == NULL)
            {
                root = root->left;
                delPtr->left = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }

            // Case 2 - 2 children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(root->right);
                RemoveNodePrivate(smallestInRightSubtree, root);
                root->key = smallestInRightSubtree;
                cout << "The root key containing key " << rootKey <<
                        " was overwritten with key " << root->key << endl;
            }
        }
        else
        {
            cout << "Can not remove root. The tree is empty\n";
        }
    }
    void RemoveMatch(Node* parent, Node* match, bool left)
    {
        if(root != NULL)
        {
            Node* delPtr = new Node;
            int matchKey = match->key;
            int smallestInRightSubtree;

            // Case 0 - 0 Children
            if(match->left == NULL && match->right == NULL)
            {
                delPtr->key = match->key;
		if(parent->left->key == match-> key)
		{
		   parent->left = NULL;
		}
		else
		{
		   parent->right = NULL;
		}
                //left == true ? parent->left = NULL : parent->right = NULL;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 1 - 1 Child
            else if(match->left == NULL && match->right != NULL)
            {
	        if(parent->left->key == match->key)
		{
		   parent->left = match->right;
		}
		else
		{
		   parent->right = match->right;
		}
                //left == true ? parent->right = match->right : parent->left = match->right;
                match->right = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }
            else if(match->left != NULL && match->right == NULL)
            {
	        if(parent->left->key == match->key)
		{
		   parent->left = match->left;
		}
		else
		{
		   parent->right = match->left;
		}
                //left == true ? parent->left = match->left : parent->right = match->left;
                parent->left = match->left;
                match->left = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 2 - 2 Children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(match->right);
                RemoveNodePrivate(smallestInRightSubtree, match);
                match->key = smallestInRightSubtree;
		cout << smallestInRightSubtree << endl;
            }
        }
        else
        {
            cout << "Can not remove match. The tree is empty\n";
        }
    }
    void RemoveSubtree(Node* ptr) //post order traversal actually
    {
        if(ptr != NULL)
        {
            if(ptr->left != NULL)
            {
                RemoveSubtree(ptr->left);
            }
            if(ptr->right != NULL)
            {
                RemoveSubtree(ptr->right);
            }
            cout << "Deleting the node containing key " << ptr->key << endl;
            delete ptr;
        }
    }

public:
    BST()
    {
        root = NULL;
    }
    ~BST()
    {
        RemoveSubtree(root);
    }
    Node* CreateLeaf(unsigned long long int key, string name) //add toprivate
    {
        Node* node_ptr = new Node;
        node_ptr->key = key; // first key is the key inside node, second key is the key being passed in
        node_ptr->name = name;
        node_ptr->left = NULL;
        node_ptr->right = NULL;

        return node_ptr;
    }

    void AddLeaf(unsigned long long int key, string name)
    {
        AddLeafPrivate(key, root, name);
    }
    void PrintInOrder()
    {
        PrintInOrderPrivate(root);
    }
    Node* ReturnNode(unsigned long long int key) //helper function //add to private string name
    {
        ReturnNodePrivate(key, root);
    }
    unsigned long long int ReturnRootKey()
    {
        if(root != NULL)
        {
            return root->key;
        }
        else
        {
            //will need to adjust if we have negative numbers
        }
    }
    void PrintChildren(unsigned long long int key)
    {
        Node* ptr = ReturnNode(key);

        if(ptr != NULL)
        {
            cout << "Parent Node = " << ptr->key << endl;
            //cout << "NAME: " << ptr->name << endl;

            ptr->left == NULL ?
            cout << "Left Child = NULL\n":  //true
            cout << "Left Child = " << ptr->left->key << endl; //false
            //cout << "NAME: " << ptr->name << endl;

            ptr->right == NULL ?
            cout << "Right Child = NULL\n":
            cout << "Right Child = " << ptr->right->key << endl;
            //cout << "NAME: " << ptr->name << endl;
        }
        else
        {
            cout << "Key " << key << " is not in the tree\n";
        }
    }
    unsigned long long int FindSmallest()
    {
        FindSmallestPrivate(root);
    }
    void RemoveNode(unsigned long long int key)
    {
        RemoveNodePrivate(key, root);
    }


};


int main()
{
    BST tree;
    int choice;
    string temp;
    unsigned long long int number;
    string name;
    //long TreeKeys[16] = {50, 76, 21, 4 ,32 ,64, 15, 52, 14, 100, 83, 2, 3, 70, 87, 80};
    //string TreeNames[16] = {"berry", "hello", "k", "s", "k", "s","berry", "hello", "k", "s",};

    cout << "Printing the tree in order\nBefore adding numbers\n";

    tree.PrintInOrder();

    cout << endl;
      /*
    do
    {
    
    cout << "Choose an option from the following (only '1', '2', '3', '4' or '5':\n";
    cout << "1. Add a new telephone number and name into the tree\n";
    cout << "2. Delete a number from the tree\n";
    cout << "3. Locate a telephone number in the tree (if it exists)\n";
    cout << "4. Print the tree\n";
    cout << "5. Quit the program\n";
    cin >> choice;
      
    switch(choice)
    {
       case 1:
          cout << "Enter telephone number (no spaces or dash, or special characters) and name into the tree: \n";
          cin >> number;
          cout << "Enter the name: ";
	  cin.ignore(1000,'\n');
          getline(cin, name);
          tree.AddLeaf(number, name);
          break;
       case 2:
          cout << "Enter number to delete (if it exists): ";
          cin >> number;
	  tree.RemoveNode(number);
          break;
       case 3:
          cout << "Locate a telephone number in the tree: ";
          cin >> number;
          break;
       case 4:
          cout << "The Tree containing your names and numbers is the following\n";
          tree.PrintInOrder();
          cout << endl;
          break;
    }
    }while(choice != 5);
    */
    
    for(long i = 0; i < 16; i++)
    {
        tree.AddLeaf(TreeKeys[i]);
    }

    cout << "Printing the tree in order\nAfter adding numbers\n";

    tree.PrintInOrder();

    cout << endl;

    //tree.PrintChildren(tree.ReturnRootKey(), blank);

    cout << endl;
    cout << endl << endl << endl;

    cout << "All the Nodes are as follows:\n\n";
    for(long i = 0; i < 16; i++)
    {
        tree.PrintChildren(TreeKeys[i]);
        cout << endl;
    }

    cout << "The smallest node is: " << tree.FindSmallest() << endl;

    cout << endl << endl;
    tree.PrintInOrder();
    cout << endl << endl;
    long input = 0;
    cout << "Enter a key value to delete. Enter -1 to stop the process\n";
    while(input != -1)
    {
        cout << "Delete Node: ";
        cin >>input;
        {
            if(input != -1)
            {
                cout << endl;
                tree.RemoveNode(input);
                tree.PrintInOrder();
                cout << endl;
            }
        }
    }
    

    return 0;
}
