/******************************************************************************
 
 
 // ATTENTION THIS QUALIFIES FOR EXC. PER Dr. Hixon
 
 
   Brandon Jones
   N534H699

       Program #8
   
   Description of problem:
      CUSTOMERS WANT SERVICED
      ONLY 5 CAHSIERS
      LINES ONLY HAS 6-7 SPACES AND REFUSE THOSE WHO WE DONT HAVE THE ROOM FOR
      
   Classes:
      Customer: STORE INFO ABOUT A GIVEN CUSTOMER
         variables to keep track of the customer information
            number- customer ordered in respect to other customers
            arrival_time- what time the customer arrives with respect to the
               store operation time
            serv_time- how much time the cashier takes to service the customer
            reg_time- the time at the rgister
         functions
            constructor- sets the needed variables to input
            getarrival_time- returns the customers arrival_time
            getserv_time- returns the customers serv_time
            incr_reg_time- increments the customers time at register
      Cashier: STORES INFO ABOUT A CASHIER'S LINE (QUEUE)
         variables:
            tot_timer- the tot_timer cashier line
            status- whether cashier is available or not
            timer- increments timer of cashiers use
         functions:
            see below
      Store_Operation: KEEPS TRACKS OF THE STORES OPERATION INFORMATION
         variables:
            see below
         functions:
            see below



*/


#include <iostream>
#include <queue>
#include <cstdlib>
#include <ctime>
#include <fstream>
using namespace std;


class Customer
{
   //number- customer ordered in respect to other customers
   int number;
   //arrival_time- what time the customer arrives with respect to the store 
     //operation time
   int arrival_time;
   //serv_time- how much time the cashier takes to service the customer
   int serv_time;
   //reg_time- the time at the rgister
   int reg_time;

public:
   Customer()
   {
      //number = 0;
      arrival_time = 0;
      serv_time = 0;
      reg_time = 0;
   }

   Customer(int arriv_t, int serv_t)
   {
      //number = num;
      arrival_time = arriv_t;
      serv_time = serv_t;
   }

   int getarrival_time()
   {
      return arrival_time;
   }

   int getserv_time()
   {
      return serv_time;
   }

   bool incr_reg_time()
   {
      reg_time++;
      if(reg_time >= serv_time)
      {
	 return true;
      }
      return false;
   }



};


class Cashier
{
   int tot_timer;
   char status;
   int timer;

public:
   Cashier()
   {
      tot_timer = 0;
      status = 'a';
      timer = 0;
   }


   /*Cashier(int t_lim)
   {
      tot_timer = 0;
      status = 'a';
      timer = t_lim;
   }*/


   //sets status to unavailable 'u'
   void cashier_unavailable()
   {
      status = 'u';
   }

   // sets status to available 'a'
   void cashier_available()
   {
       status = 'a';
   }

   //returns status
   char getstatus()
   {
      return status;
   }

   /*bool dec_timer()
   {
      timer--;
      if(timer <= 0)
      {
	 return true;
      }
      return false;
   }
   */

   //increments the time
   void incr_timer()
   {
       timer++;
   }

   //returns the timer
   int get_timer()
   {
       return timer;
   }

   //resets timer to 0
   void reset_timer()
   {
       timer = 0;
   }

   //increments total timer
   void incr_tot_timer()
   {
      tot_timer++;
   }


};


//This might be obsolete (ONLY STORES VALUES)
class Store
{
   //tracks number of customers services
   int num_serviced;
   //tracks total wait time for a customer
   long tot_wait_time;
   //tracks number of customers turned away
   int num_turned_away;

public:
   //default constructor
   Store()
   {
      num_serviced = 0;
      tot_wait_time =0;
      num_turned_away = 0;
   }
   
   //increases customers serviced
   void incr_cust_served()
   {
      num_serviced++;
   }

   //returns customers serviced
   int getcust_served()
   {
      return num_serviced;
   }

   //increases total wait time
   void inc_tot_wait_time(int var)
   {
      tot_wait_time += var;
   }

   //gets total wait time
   long gettot_wait_time()
   {
      return tot_wait_time;
   }

   //increases number turned away
   void inc_num_turned_away()
   {
      num_turned_away++;
   }

   //returns number of customers turned away
   int getnum_turned_away()
   {
      return num_turned_away;
   }



};


//main function (entry point)
int main(void)
{
   //queue of ints (service times) in an array (5 lines)
   queue<Customer> cashier[5];
   //queue of customers, stores all customers in input file
   queue<Customer> allcustomers;
   //array of cashiers, the clerks who ring out the customers
   Cashier clerk[5];
   //operation_summary is the object we use to interact with Store class
   Store operation_summary;
   //checks if we can add
   bool canadd = false;
   //checks if a cashier is available
   bool cashier_call[5];
   //checks to see if we have more customers at a given time
   bool more = false;
   //checks to see if all clerks are busy
   bool all_busy;
   //sets a value of the smallest line (index value)
   int smallest_line = 0;
   
   int cust_wait_time = 0;

   //reads from an input file
   ifstream read( "output.dat" );
   cout << "Looking for 'output.dat' file name" << endl;

   //checks to see if file opens succesfully
   if( read.fail() )
   {
      cout << "Failure to open" << endl;
      cout << "Program will end" << endl;
      return -1;
   }
   
   //variabels to store the read in file values
   int trash;
   int arrival_time;
   int serv_time;
   
   
   //reads in each of the values of the customer
   do
   {
      read >> trash;
      if( !read.eof() )
      {
         read >> arrival_time;
         read >> serv_time;
         Customer temp = Customer(arrival_time, serv_time);
         allcustomers.push( temp );
      }
   }while( !read.eof() );
   
   if( allcustomers.empty() )
   {
      cout << "SLOW DAY AT WORK" << endl;
      return -1;
   }
   
   //gives the all customers size
   cout << "Today will have " << allcustomers.size() << " customers" << endl;
   


      /* We need to know the following:
       * 1. which lines(queue) we can add to, and which lines we can not add
       * to
       * 2. which cashiers (clerks) can service the next customer in the 
       * line(queue)
       * 3. which lines(queue) are completely empty
       * 4. if the store is empty (all lines(queues) are completely empty)
       */
   cout << endl;



   //debug is used for minuet by minuet status updates and checks
   bool debug_mode = false;
   //loop of store times
   
   for(int time = 0; time < 600; time++)
   {

      canadd = false;
      int smallest = 10;

      //cycles through each line.
      for(int line = 0; line < 5; line++)
      {
         //finds smallest
         //look for smallest line
         if( cashier[line].size() < smallest )
         {
            smallest = cashier[line].size();
            smallest_line = line;
         }
         //checks to see if any line is below 7 customers
         if(cashier[line].size() < 7)
         {
            canadd = true;
            if(debug_mode)
            {
               cout << "Cashier's queue " << line <<" has room!"<<endl;
            }
         }
      }


      //if we can add to a line...
      if(canadd)
      {

	 //checks to see if a customer is ready to enter store
         if( time == allcustomers.front().getarrival_time() )
         {
	    //only adds under 570 minuets
            if(time < 570)
            {
               cashier[smallest_line].push(allcustomers.front());
               if(debug_mode)
               {
                  cout<<"Cashier "<<smallest_line;
		  cout <<" is servicing a customer at time "<<time<<endl;
               }
               allcustomers.pop();
            }
            else
            {
	       //if customer arrived too late... POP
               allcustomers.pop();
               operation_summary.inc_num_turned_away();
               cout << "Customer arrived to late, TURNED DOWN" << endl;
            }
         }

           //if there is another customer at the given minuet, we need to add more
           if( time == allcustomers.front().getarrival_time() )
           {
              more = true;
           }
           //while we have more to add
           while(more)
           {
              smallest = 10;
              canadd = false;
              for(int line = 0; line < 5; line++)
              {
                 //finds smallest
                 if( cashier[line].size() < smallest )
                 {
                    smallest = cashier[line].size();
                    smallest_line = line;
                 }
                 if(cashier[line].size() < 7)
                 {
                    canadd = true;
                    if(debug_mode)
                    {
                       cout << "Cashier's queue " << line << " has room!" << endl;
                    }
                }
            }
            //add
            if(canadd)
            {
               if( time == allcustomers.front().getarrival_time() )
               {
                  if(time < 570)
                  {
                     cashier[smallest_line].push( allcustomers.front() );
                     if(debug_mode)
                     {
                        cout << "Cashier " << smallest_line;
			cout << " is servicing a customer at time " << time << endl;
                     }
                     allcustomers.pop();
                  }
                  else
                  {
                     allcustomers.pop();
                     operation_summary.inc_num_turned_away();
                     cout << "Customer arrived too late, TURNED DOWN" << endl;
                  }
               }
            }
            //checks if we have more to add
            if( time == allcustomers.front().getarrival_time() )
            {
               more = true;
            }
            else
               more = false;
            }

       }
       //if we can not add the customer gets rejected
       else
       {
          allcustomers.pop();
          operation_summary.inc_num_turned_away();
          cout << "a customer was turned away, lines full" << endl;
       }
       
       //cycles through the cashiers
       for(int i = 0; i < 5; i++)
       {
	  //if any cashier line is not empty then the front of that line is being
	  //serviced and we need to take care of that customer
          if( !cashier[i].empty() )
          {
             clerk[i].incr_timer();
             clerk[i].cashier_unavailable();
             if( clerk[i].get_timer() == cashier[i].front().getserv_time() )
             {
	        cust_wait_time = time - ( cashier[i].front().getserv_time() + cashier[i].front().getarrival_time() );
		operation_summary.inc_tot_wait_time(cust_wait_time);
                cashier[i].pop();
                operation_summary.incr_cust_served();
                clerk[i].reset_timer();
                clerk[i].cashier_available();
                if(debug_mode)
                {
		   cout << "customer wait time: " << cust_wait_time << endl;
                   cout << "customer done" << endl;
                }
             }
          }
       }
     //cycles through lines
     for(int line = 0; line < 5; line++)
     {
        //checks the status of a given clerk
        if(clerk[line].getstatus() == 'a')
        {
	   //if available the cashier is calling for next
	   cashier_call[line] = true;
	   if(debug_mode)
	   {
	       //cout << "Cashier at lane " << line << " is 
	       //CALLING FOR NEXT!" << endl;
	   }
	}
	else
	{
	   //else the cashier is busy and not calling for next
	   cashier_call[line] = false;
	   if(debug_mode)
	   {
	      //cout << "Cashier at lane " << line << " is
	      // SERVING A CUSTOMER" << endl;
	   }
	}

     }

     //cycles through each cashier_call availability
     for(int j = 0; j < 5; j++)
     {
        //if cashier_call is always false we then know that
        //we know that all busy is true;
        if(cashier_call[j] = true)
        {
           all_busy = false;
        }
        //returns cashier_call to false
        cashier_call[j] = false;
     }

     //if all busy then we will want to increment total wait time
     /*
     if(all_busy)
     {
        operation_summary.inc_tot_wait_time();
     }
     */
     
     //ends minuet and displays a broken up block of a minuet
     if(debug_mode)
     {
        cout << "END OF MINUET" << endl;
     }
     
   }
   //step
   //checks to see if anyone still left in file
   if(debug_mode)
   {
      if(!allcustomers.empty())
      {
         cout << " NOT EMPTY " << endl;
         cout << allcustomers.front().getarrival_time() << endl;
         allcustomers.pop();
      }
   }


   //folloiwng calculates the needed store operation variables
   
   //finds out the customer serviced is
   int cust_serviced = operation_summary.getcust_served();
   //finds out the total customers turned away
   int total_turned_away = operation_summary.getnum_turned_away();
   //finds the divsor needed to calculate average wait time
   double divisor = (cust_serviced + total_turned_away);
   //calculates average wait time
   double average_wait_time = ( operation_summary.gettot_wait_time() ) / divisor;
   average_wait_time += 1.0;
   //SHOULD BE TOTAL CUSTOMERS ON RHS of / sign and should give AVERAGE!!

   
   //provides output for our wanted variables
   cout << "Total number of customers serviced: " << cust_serviced << endl;
   cout << "Total number of customers turned away: " << total_turned_away << endl;
   cout << "Average wait time: " << average_wait_time<< endl;
   //might produce a time that is a "minuet" below the correct, PER RENE
   //IT IS CORRECT PER DR. HIXON


return 0;
}