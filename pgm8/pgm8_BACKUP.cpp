#include <iostream>
#include <queue>
#include <cstdlib>
#include <ctime>
#include <fstream>
using namespace std;


class Customer
{
   int number;
   int arrival_time;
   int serv_time;
   int reg_time;
   
public:
   Customer()
   {
      //number = 0;
      arrival_time = 0;
      serv_time = 0;
      reg_time = 0;
   }
   
   Customer(int arriv_t, int serv_t)
   {
      //number = num;
      arrival_time = arriv_t;
      serv_time = serv_t;
   }
   
   int getarrival_time()
   {
      return arrival_time;
   }
   
   int getserv_time()
   {
      return serv_time;
   }
   
   bool incr_reg_time()
   {
      reg_time++;
      if(reg_time >= serv_time)
      {
	 return true;
      }
      return false;
   }
   
   
   
};


class Cashier
{
   int tot_timer;
   char status;
   int timer;
   
public:
   Cashier()
   {
      tot_timer = 0;
      status = 'a';
      timer = 0;
   }
   
   
   Cashier(int t_lim)
   {
      tot_timer = 0;
      status = 'a';
      timer = t_lim;
   }
   
   
   void cashier_unavailable()
   {
      status = 'u';
   }
   
   char getstatus()
   {
      return status;
   }
   
   bool dec_timer()
   {
      timer--;
      if(timer <= 0)
      {
	 return true;
      }
      return false;
   }
   
   void incr_timer()
   {
      tot_timer++;
   }
   
   
};


//This might be obsolete (ONLY STORES VALUES)
class Store
{
   int time_cnt;
   int num_serviced;
   long tot_wait_time;
   int num_turned_away;
   
public:
   Store()
   {
      time_cnt = 0;
      num_serviced = 0;
      tot_wait_time =0;
      num_turned_away = 0;
   }
   
   void incr_cust_served()
   {
      num_serviced++;
   }
   
   int getcust_served()
   {
      return num_serviced;
   }
   
   void inc_tot_wait_time()
   {
      tot_wait_time++;
   }
   
   long gettot_wait_time()
   {
      return tot_wait_time;
   }
   
   void inc_num_turned_away()
   {
      num_turned_away++;
   }
   
   int getnum_turned_away()
   {
      return num_turned_away;
   }
   
   
   
};

/*
Customer shortest_queue(queue<Customer> &cashier)
{
   char shortest;
   
   for(int i = 0; i < 5; i++)
   {
      if( cashier[i].size()
   }
   
}
*/




int main(void)
{
   queue<int> cashier[5];
   queue<Customer> allcustomers;
   queue<int> arrival_times;
   queue<int> serviced_times;
   Cashier clerk[5];
   Store operation_summary;
   bool canadd = false;
   bool cashier_call[5];
   bool store_empty = true;
   bool line_empty[5];
   int smallest_line;
   //bool cashier_free[5];
   
   ifstream read( "output.dat" );
   
   if( read.fail() )
   {
      cout << "Failure to open" << endl;
      cout << "Program will end" << endl;
      return -1;
   }
   
   int trash;
   int arrival_time;
   int serv_time;
   while( !read.eof() )
   {
      read >> trash;
      read >> arrival_time;
      read >> serv_time;
      
      Customer temp = Customer(arrival_time, serv_time);
      allcustomers.push( temp );
   }
   
   
   //BETWEEN
   /*
   while( !allcustomers.empty() )
   {
      cout << allcustomers.front().getarrival_time() << " " << allcustomers.front().getserv_time() << endl;
      allcustomers.pop();
   }
   
   
   allcustomers.front();
   
   cout << allcustomers.front().getarrival_time() << endl;
   allcustomers.pop();
   
   cout << endl << endl;
   
   //BETWEEN
   */
   
   
   arrival_times.push( allcustomers.front().getarrival_time() );
   //cout << arrival_times.front();
   //cout << endl;
   serviced_times.push( allcustomers.front().getserv_time() );
   //cout << serviced_times.front();
   //cout << endl;
   
   
   cout << endl;
   
   bool debug_mode = true;
   for(int time = 0; time < 15; time++)
   {
      
     for(int lane = 0; lane < 5; lane++)
      {
         if(store_empty)
         {
	    cashier[0].push( allcustomers.front().getserv_time() );
	    
	    
	    //operation_summary.incr_cust_served();
	    //clerk[0].cashier_unavailable();
         }
      }
         
      /*for(int lane = 0; lane < 5; lane++)
      {
         if( clerk[lane].dec_timer() )
            {
	       operation_summary.incr_cust_served();
	       if(debug_mode)
	       {
	          cout << "Customer Serviced at time" << time << endl;
	       }
	       //clerk[lane].
	    }
      }
      */
     
     
     /* from this for loop we will now know the following:
       * 1. which lines(queue) we can add to, and which lines we can not add to
       * 2. which cashiers (clerks) can service the next customer in the line(queue)
       * 3. which lines(queue) are completely empty
       * 4. if the store is empty (all lines(queues) are completely empty)
       */
      
      
      //if( time = cashier[0].front )
      canadd = false;
      int smallest = cashier[0].size();
      for(int line = 0; line < 5; line++)
      {
	 
	 //finds smallest
	 if(cashier[line].size() < smallest)
	 {
	    //smallest = cashier[line];
	    smallest_line = line;
	 }
	 //1.
	 if(cashier[line].size() < 7)
	 {
	    canadd = true;
	    //below
	    if(debug_mode)
	    {
	       cout << "Cashier's queue " << line << " has room!" << endl;
	    }
	    //above
	 }
	 
	    //above
      }
      
	 
      for(int line = 0; line < 5; line++)
      {
	 //2.
	 if(clerk[line].getstatus() == 'a')
	 {
	    cashier_call[line] = true;
	     //below
	    if(debug_mode)
	    {
	       cout << "Cashier at lane " << line << " is CALLING FOR NEXT!" << endl;
	    }
	    //above
	 }
	 else
	 {
	    cashier_call[line] = false;
	     //below
	    if(debug_mode)
	    {
	       cout << "Cashier at lane " << line << " is SERVING A CUSTOMER" << endl;
	    }
	    //above
	 }
	 //3.
	 if( cashier[line].size() == 0 )
	 {
	    line_empty[line] = true;
	     //below
	    if(debug_mode)
	    {
	       cout << "Lane " << line << " is EMPTY!" << endl;
	    }
	    //above
	 }
	 else
	 {
	    line_empty[line] = false;
	     //below
	    if(debug_mode)
	    {
	       cout << "Lane " << line << " is NOT EMPTY!" << endl;
	    }
	    //above
	 }
	 //4.
	 if( !line_empty[line] )
	 {
	    store_empty = false;
	     //below
	    if(debug_mode)
	    {
	       cout << "Store is NOT EMPTY!" << endl;
	    }
	    //above
	 }
	 
	 
	 
      }
      
      
      
      
      
      
      
   
   //incr_cust_served();
   //inc_tot_wait_time();
   //inc_num_turned_away();
   
   }
   
   //CHECK TO MAKE SURE AVERAGE IS CORRECT
   //CHECK TO MAKE SURE num_turned_away + num_serviced = allcustomers.size() !!
   int cust_serviced = operation_summary.getcust_served();
   int total_turned_away = operation_summary.getnum_turned_away();
   double divisor = (cust_serviced + total_turned_away);
   double average_wait_time = ( operation_summary.gettot_wait_time() ) / divisor; //SHOULD BE TOTAL CUSTOMERS ON RHS of / sign and should give AVERAGE!!
   
   cout << "Total number of customers serviced: " << cust_serviced << endl;
   cout << "Total number of customers turned away: " << total_turned_away << endl;
   cout << "Average wait time: " << average_wait_time << endl;
   
   
   
}