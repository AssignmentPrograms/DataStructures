/******************************************************************************
 
   Brandon Jones
   N534H699

       _TEST EC
   
   Description of problem:
      We wish to iterate through a list using an iterator and ORDERING IT
   


*/


#include <iostream>
#include <list>
#include <cstdlib>
using namespace std;

//void iterate(int input);





   
int main(void)
{
  
   //user input
   int input;
   //created list
   list<int> num;
   //used to store value inside list
   int curr;
   
   cout << "This is the ORDERED LIST" << endl;
   cout<<"There will be a linked list of 5 numbers that will be ordered"<<endl;
   cout << "Insert the first number" << endl;
   
   //get input and then add it to the front of list
   cin >> input;
   num.push_front(input);
   
   //creates an iterator and sets it to the begining
   list<int>::iterator pos = num.begin();
   
   //ran 4 times, for 4 input values
   
   //THIS IS THE PROBLEM 10 VERSION!!!!!!!!!!!!!!!
   //ORDERED LIST
   for(int i = 0; i< 4; i++)
   {
     
      //user inserts next number and our iterator starts at the begining
      cin >> input;
      pos = num.begin();
      bool done = false;
      
      //ran while our iterator does not raech the end
      while( pos != num.end() )
      {
	 //set a variable to the value to where our iterator is pointing to
         curr = *pos;
	 //compares data
         if(input <= curr && !done)
         {
            num.insert(pos, input);
            done = true;
         }
         pos++;
      
      }
   
   
      //if we made it through the while loop without ever changing done
      if(!done)
      {
         num.push_back(input);
      }
   
   }
   
   cout << endl;
   //iterator starts at begining and iterates through printing out values
   pos = num.begin();
   
   for(int i = 0; i < 5; i++)
   {
   cout << *pos << endl;
   pos++;
   }
   
   cout << endl << endl;
   
   
   
   
   //UNORDEDED LIST SECTION!!!!!!!!!!!!!!!!!!!!
   list<int> ulist;
   list<int>::iterator it = ulist.begin();
   
   cout << "This is the UNORDERED LIST" << endl;
   cout << "Enter 5 values into an UNORDERED list" << endl << endl;
   //cin >> input;
   //ulist.push_back(input);
   
   for(int i = 0; i < 5; i++)
   {
      cin >> input;
      ulist.push_back(input);
   }
   cout << endl;
   it = ulist.begin();
   for(int i = 0; i < 5; i++)
   {
      curr = *it;
      cout << curr << endl;
      it++;
   }
   
   
   
}

/*
void iterate(int input)
{
   if(input == pos)
   {
      pos = input;
   }
   else if(input > *pos)
   {
      pos = input;
   }
   else
   {
      pos++;
   }
   
   
}*/

/*
while(!num.end())
{
  curr = *pos;
  if(input >= pos && !done)
  {
     num.insert(input);
     done = true;
  }
 
  pos++
}
*/


   
   
   /*cout << "Insert the second number" << endl;
   cin >> input;
   pos = num.begin();
   if(input < *pos)
   {
      *pos = input;
      num.insert(pos, input);
   }
   else
   {
      num.insert(pos, input);
   }
   
   
   cout << "Insert the third number" << endl;
   cin >> input;
   pos = num.begin();
   if(input < *pos)
   {
      *pos = input;
      num.insert(pos, input);
   }
   else
   {
      num.insert(pos, input);
   }
   
   
   
   cout << "Insert the fourth number" << endl;
   cin >> input;
   pos = num.begin();
   if(input < *pos)
   {
      *pos = input;
      num.insert(pos, input);
   }
   else
   {
      num.insert(pos, input);
   }*/

