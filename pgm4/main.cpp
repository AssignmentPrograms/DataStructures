/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #4
   
   Description of problem:
      There is a library of books being read by the user, the user enters in
      the books they are reading and the authors. The programmer has created
      a linked list to better track and output those books.
   


*/

#include <iostream>
#include <string>
#include "linked_list.hpp"
#include "node.hpp"
using namespace std;

int main(void)
{
   string book; //stores the users desired book entry
   string author; //stores the users desired author entry
   linked_list library; // the object of our class
   
   // This prompts for the user to enter a book and author
   // and quit by entering end into author
   cout << "Enter 'end' as the 'title' to quit" << endl;
   cout << "Enter in a title of book to be stored: ";
   getline (cin, book);
   cout << "Enter the author of the book: ";
   getline (cin, author);
   
   //loop to allow the user to enter multiple books and stored
   //them into the nodes, continuing until the user enteres end
   while(book != "end" || author != "end")
   {
      
      library.insert_node(book, author);
      cout << endl;
      cout << "Enter 'end' to quit" << endl;
      cout << "Enter in a title of book to be stored: ";
      getline (cin, book);
      cout << "Enter the author of the book: ";
      getline (cin, author);
      //cin.ignore(10000, '\n');
      cout << endl;
   }
   
   //prints out the entered users input
   library.traverse_nodes();
   
   cout << endl;
   
}