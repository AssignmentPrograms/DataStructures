/****************************************************************************** 
   Brandon Jones
   N534H699

       Program #4
   
  Description of problem:
     There is a library of books being read by the user, the user enters in
     the books they are reading and the authors. The programmer has created
     a linked list to better track and output those books.
      
  Psudo Code
     Class Name: linked_list
     Data:       book - used to store the user entered book title in our node
		 author - used to store the user entered author in our node
		 *link - point that connects one 
		    node to another (points to the enxt node)
     Operation Functions:
                 Node() - default constructor, setting book, author and link
                    to NULL
                 Node(string book, string author) - constructor for user input
                 set_link(Node*ptr) - this sets the link to be the node pointer
                    we create to connect two nodes
                 Node *get_link() - this is to return the current link
                 process_data() - this is responsible for outputting the
                    information the user has entered within the node
*/

#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

class Node
{
   //contents of node
   string book; //used to store the users entered book title
   string author; //used to store the users entered book's author
   Node *link;  // link -> 'next' points to next link in list
   
public:
   //constructors
   Node();
   Node(string book, string author);   
   
   //Functions
   //mutator
   void set_link(Node *ptr);
   //accessor
   Node *get_link();
   void process_data();
   
};

#endif