/****************************************************************************** 
   Brandon Jones
   N534H699

       Program #4
   
  Description of problem:
     There is a library of books being read by the user, the user enters in
     the books they are reading and the authors. The programmer has created
     a linked list to better track and output those books.
      
  Psudo Code
     Class Name: linked_list
     Data:       *head - pointer to the begining of the linked list
		 *tail - pointer to the end of the linked list
     Operation Functions:
                 insert_node - used to insert a new node to the end of the list
                 traverse_nodes - used to reffer to the 
		    node class and print the list


*/

#ifndef LINKED_LIST_H
#define LINKED_LIST_H
#include "node.hpp"

class linked_list
{
   Node *head, *tail;
  
public:
   //Constructors
   linked_list();
   
   //Operation Functions
   void insert_node(string book, string author);
   void traverse_nodes();
   
   
   
};




#endif