/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #4
   
   Description of problem:
      There is a library of books being read by the user, the user enters in
      the books they are reading and the authors. The programmer has created
      a linked list to better track and output those books.
   


*/

#include <iostream>
#include <string>
#include <cstdlib>
#include "node.hpp"
using namespace std;


   Node::Node()
   {
      book = "";
      author = "";
      link = NULL;
   }
   
   Node::Node(string b, string a)
   {
      book = b;
      author = a;
      link = NULL;
   }
   
   void Node::set_link(Node *ptr)
   {
      link = ptr;
   }
   
   Node *Node::get_link()
   {
      return link;
   }
   
   void Node::process_data()
   {
      cout << "You are reading/read: " << book;
      cout << " By: " << author << endl;
   }