/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #4
   
*/

#include <iostream>
#include <string>
#include <cstdlib>
#include "linked_list.hpp"
#include "node.hpp"
using namespace std;

   linked_list::linked_list()
   {
      head = tail = NULL;
   }

   void linked_list::insert_node(string book, string author)
   {
      Node *node_ptr = new Node(book, author);
      
      if(head == NULL)
      {
         tail = head = node_ptr;
      }
      else
      {
	 tail->set_link(node_ptr);
	 tail = node_ptr;
      }
      
      
   }
   
   void linked_list::traverse_nodes()
   {
      Node *temp;
      temp = head;
      while(temp != NULL)
      {
	 temp->process_data();
	 temp = temp->get_link();
      }
      
      
   }