// Name: Brandon Jones
// Class: cs300
// Assignment: pgm1.cpp



#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;


struct inventory // structure for tracking inventory item data
{
    string description; // structure member variable to store the description of an item
    int quantity; // structure member variable that stores the amount of an item
    float price_per_item, total_cost; // structure member variable that stores price per item and then calculates total costs to be built up for a later displayed sum
};




int main()
{
    char answer; // stores user response for do while loop to continue, and continue storing inventory items
    inventory items; // makes a structure member to be used for the item data
    items.total_cost = 0; // sets the total_cost to 0 so we can build up a sum each time through our do while loop, for all items

    ofstream records; // opens file to be used to store our data information for items
    records.open("inventory.txt");
    if(records.fail()){
        exit(EXIT_FAILURE);
    }



      do{
      cout << "Describe the item: ";
      getline(cin, items.description);
      records << items.description << " ";
      cout << endl;
      cout << "Number of this item in inventory: ";
      cin >> items.quantity;
      records << '\n' << '\t' << "Number of items: " << items.quantity << " ";
      cout << endl;
      cout << "Enter price for one of this item: $";
      cin >> items.price_per_item;
      records << "Price for one: $" << items.price_per_item << " ";
      cout << endl;
      records << "Total cost for this item ==> $" << (items.price_per_item * items.quantity) << endl;
      items.total_cost += items.price_per_item * items.quantity;
      cout << endl;
      cout << "Record recorded, another? ('y' = yes, 'n' = no)";
      toupper(answer);
      cin >> answer;
      cin.ignore(10000, '\n');
      cout << endl;
      records << endl;
      }while(answer != 'n');



    records.close();
    records << "Total cost of all your items: " << items.total_cost;
    cout << "Total cost of your items: " << items.total_cost;
    cout << endl;
    return 0;
}