/****************************************************************************** 
   Brandon Jones
   N534H699

       Program #4
   
  Description of problem:
     There is a library of books being read by the user, the user enters in
     the books they are reading and the authors. The programmer has created
     a linked list to better track and output those books.
      
  Psudo Code
     Class Name: linked_list
     Data:       *head - pointer to the begining of the linked list
		 *tail - pointer to the end of the linked list
     Operation Functions:
                 insert_node - used to insert a new node to the end of the list
                 traverse_nodes - used to reffer to the 
		    node class and print the list


*/


#ifndef LINKED_LIST_H
#define LINKED_LIST_H
#include "node.hpp"
#include <iostream>
using namespace std;

//#include <cstdlib>

template<class Type>
class linked_list
{
   Node<Type> *head, *tail; // tracks the begining/end of list

public:

   linked_list(); //creates an empty list
   void insert_node(void); //error handling w/empty paramaters
   void insert_node(Type data1); // inserts a new node
   void transverse_node(); //prints data we wish for

};


//code for the following explained above
template<class Type>
linked_list<Type>::linked_list()
{
   head = tail = NULL; // sets head and tail to NULL
}

template<class Type>
void linked_list<Type>::insert_node( void )
{
  // throw std::exception("No input received")
  // error handling
};

template<class Type>
void linked_list<Type>::insert_node(Type data1)
{
   //create a node point and set it equal to a new node with data1
   Node<Type> *node_ptr = new Node<Type>(data1); 
     
   if(head == NULL)
   {
      // set that new node equal to tail and head if an empty list
      tail = head = node_ptr; 
   }
   else
   {
      // if not empty list add node to end of list
      tail->set_link(node_ptr);
      tail = node_ptr;
   }
   
   
   
}

template<class Type>
void linked_list<Type>::transverse_node()
{
   //create a temp variable to store head node
   Node<Type> *temp;
   temp = head;
   //if head == NULL - empty list
   //process the data for the list, going through each node
   while(temp != NULL)
   {
      temp->process_data();
      temp = temp->get_link();
   }
}





#endif