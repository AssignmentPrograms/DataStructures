/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #5
   
   Description of problem:
      We wish to use templates to allow for a wide range of data types with the
      same class in use, as opposed to writing the same class multiple times
     
   Psudo Code
     Class Name: Node
     Data:       data1 - the variable to store the data of type (WHATEVER)...
		 *link - point that connects one 
		    node to another (points to the next node)
     Operation Functions:
                 Node() - default constructor, setting book, author and link
                    to NULL
                 Node(Type data1) - is the type we wish, but will construct our
                    list with the inserted data and make it the first.
                 set_link(Node<Type>*ptr) - this sets the link to be the 
                    node pointer we create to connect two nodes
                 Node *get_link() - this is to return the current link
                 process_data() - this is responsible for outputting the
                    information the user has entered within the node


*/


#ifndef NODE_H
#define NODE_H
#include <iostream>
using namespace std;

//#include <cstdlib>

template<class Type> // used throughout to allow for mult. data types
class Node
{
   Type data1;  //The variable to store processing data we desire
   Node<Type> *link; //the link between two nodes
   
public:
   
   Node(); // default constructor
   Node(Type data1); // constructor
   void set_link(Node<Type> *ptr); //sets the link to a pointer, 2 b accesbl
   Node *get_link(); // returns the link
   void process_data(); //print the data
};


//the following are explained above
template<class Type>
Node<Type>::Node()
{
    data1 = NULL; // sets data1 to NULL upon node creation
    link = NULL; // sets link to NULL upon node creation
}

template<class Type>
Node<Type>::Node(Type a)
{
   data1 = a; // sets our variable data1 to passed through data
}

template<class Type>
void Node<Type>::set_link(Node<Type> *ptr)
{
   link = ptr; // sets link to ptr to access private data
}

template<class Type>
Node<Type> *Node<Type>::get_link()
{
   return link; // returns private link to be used
}

template<class Type>
void Node<Type>::process_data()
{
   cout << "Data: " << data1 << endl; // prints out data
}




#endif