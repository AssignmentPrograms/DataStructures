/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #5
   
   Description of problem:
      We wish to use templates to allow for a wide range of data types with the
      same class in use, as opposed to writing the same class multiple times
   


*/


#include <iostream>
//#include <cstdlib>
#include "node.hpp"
#include "linked_list.hpp"
using namespace std;

int main(void)
{
   //int int_data;
   //double double_data;
   linked_list<int> ent; // integer holder
   linked_list<double> dawble; // double holder
   linked_list<long> lawng; // long variable
   linked_list<char> chawr; // char variable
   linked_list<float> flowt; // float variable
   linked_list<string> strang; // string variable
   
   /*cout << "Enter in an integer: ";
   cin >> int_data;
   cout << "Enter in a double: ";
   cin >> double_data;*/
   
   //prints out integers
   cout << "The following are of 'int' type:" << endl;
   ent.insert_node(8);
   ent.insert_node(2);
   ent.insert_node(6);
   ent.insert_node(8);
   ent.insert_node(15);
   ent.transverse_node();
   
   cout << endl;
   
   //prints out doubles
   cout << "The following are of 'double' type:" << endl;
   dawble.insert_node(12.4);
   dawble.insert_node(14.4);
   dawble.insert_node(13.9);
   dawble.insert_node(2.5);
   dawble.insert_node(1452224.566);
   dawble.transverse_node();
   
   cout << endl;
   
   //prints out longs
   cout << "The following are of 'long' type:" << endl;
   lawng.insert_node(-2151566);
   lawng.insert_node(186549698);
   lawng.insert_node(995);
   lawng.insert_node(4255);
   lawng.insert_node(649);
   lawng.transverse_node();
   
   cout << endl;
   
   //prints out chars
   cout << "The following are of 'char' type:" << endl;
   chawr.insert_node('a');
   chawr.insert_node('B');
   chawr.insert_node('c');
   chawr.insert_node('5');
   chawr.insert_node('/');
   chawr.transverse_node();
   
   cout << endl;
   
   //prints out floats
   cout << "The following are of 'float' type:" << endl;
   flowt.insert_node(4548.46848);
   flowt.insert_node(4888.1856);
   flowt.insert_node(55.2);
   flowt.insert_node(64.888);
   flowt.insert_node(.255);
   flowt.transverse_node();
   
   cout << endl;
   
   //prints out strings
   cout << "The following are of 'string' type:" << endl;
   strang.insert_node("hello");
   strang.insert_node("BlehBleh");
   strang.insert_node("set");
   strang.insert_node("huttt");
   strang.insert_node("OMAHAAAH!");
   strang.transverse_node();
   
   cout << endl;
   
   
}