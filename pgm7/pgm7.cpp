/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #7
   
   Description of problem:
      We wish to convert decimal to binary and octal. However by default the
      result would be printed out backwards. Using the stack library we are
      able correct the order.
   


*/


#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <stack>
using namespace std;

//This function will recieve the input from the user that will be later
//	converted.
int getint();
//This function sends in the return value of getint() and converts to binary
int toBinary(int decimal);
//This function sends in the return value of getint() and converts to Octal
int toOctal(int decimal);


int main(void)
{
   int test; //Used to store the decimal the user enters
   
   
   //loop to allow repeatadility to convert as many number as wanting, till -1
   while(test != -1)
   {
      cout << endl;
      cout << "_______________________________" << endl;
      cout << "Enter a number to be converted: " << endl;
      test = getint();
      cout << endl;
      //tests if the decimal number is neg, and if it is -1
      if(test <= 0)
      {
	 if(test == -1)
	 {
	    cout << endl;
	 }
	 else
	 {
	    cout << "Only positive numbers please" << endl;
	 }
      }
      //if does not pass previous tests then will print out conversions
      else
      {
         cout << "The Binary Representation: ";
         toBinary(test);
         cout << endl;
         cout << endl;
         cout << "The Octal Representation: ";
         toOctal(test);
         cout << endl;
      }
   }
   
   
}


int getint()
{
   //stores decimal valued number
   int decimal;
   cin >> decimal;
   return decimal;
}

//converts to binary
int toBinary(int decimal)
{
   //stores input on a stak to easily list off the elements
   stack<int> stak;
   int remainder;
   stringstream ordered;
   //does conversion
   while(decimal != 0)
   {
      remainder = decimal % 2;
      decimal /= 2;
      stak.push(remainder);
   }
   while( !stak.empty() )
   {
      ordered << stak.top();
      stak.pop();
   }
   cout << ordered.str();
}

//converts to octal
int toOctal(int decimal)
{
   int remainder;
   stack<int> stak;
   stringstream ordered;
   while(decimal != 0)
   {
      remainder = decimal % 8;
      decimal /= 8;
      stak.push(remainder);
   }
   while( !stak.empty() )
   {
      ordered << stak.top();
      stak.pop();
   }
   cout << ordered.str();
}