#ifndef ITERATOR_H
#define ITERATOR_H
#include "node.hpp"
#include <iostream>
using namespace std;

template<class Type>
class Iterator
{
   Node<Type> *pos;
   Node<Type> *end;
   
public:
   Iterator();
   void link(Node<Type> *newpos, Node<Type> *newend);
   bool operator++(int temp);
   Node<Type> *get();
   //void operator*(Node<Type> decrement);
};

template<class Type>
Iterator<Type>::Iterator()
{
   pos = end = NULL;
}

template<class Type>
void Iterator<Type>::link(Node<Type> *newpos, Node<Type> *newend)
{
   pos = newpos;
   end = newend;
}

template<class Type>
bool Iterator<Type>::operator++(int temp)
{
   if(pos != end)
   {
      //pos = pos->link;
      pos = pos->get_link();
      return true;
   }
   else
   {
      return false;
   }
   //Node<Type> *node_ptr = new Node<Type>(increment);
   //return increment;
}

template<class Type>
Node<Type>* Iterator<Type>::get()
{
   return pos;
}

#endif