#include <iostream>
#include <string>
#include "linked_list.hpp"
using namespace std;

int main(void)
{
      //int int_data;
   //double double_data;   
   /*cout << "Enter in an integer: ";
   cin >> int_data;
   cout << "Enter in a double: ";
   cin >> double_data;*/
   
   
   linked_list<int> ent; // integer holder
   //prints out integers
   cout << "The following are of 'int' type:" << endl;
   ent.push_back(8);
   ent.push_back(2);
   ent.push_back(6);
   ent.push_back(8);
   ent.push_back(15);
   ent.transverse_node();
   
   cout << endl;
   
   
   linked_list<double> dawble; // double holder
   //prints out doubles
   cout << "The following are of 'double' type:" << endl;
   dawble.push_back(12.4);
   dawble.push_back(14.4);
   dawble.push_back(13.9);
   dawble.push_back(2.5);
   dawble.push_back(1452224.566);
   dawble.transverse_node();
   
   cout << endl;
   
   linked_list<long> lawng; // long variable
   //prints out longs
   cout << "The following are of 'long' type:" << endl;
   lawng.push_back(-2151566);
   lawng.push_back(186549698);
   lawng.push_back(995);
   lawng.push_back(4255);
   lawng.push_back(649);
   lawng.transverse_node();
   
   cout << endl;
   
   linked_list<char> chawr; // char variable
   //prints out chars
   cout << "The following are of 'char' type:" << endl;
   chawr.push_back('a');
   chawr.push_back('B');
   chawr.push_back('c');
   chawr.push_back('5');
   chawr.push_back('/');
   chawr.transverse_node();
   
   cout << endl;
   
   linked_list<float> flowt; // float variable
   //prints out floats
   cout << "The following are of 'float' type:" << endl;
   flowt.push_back(4548.46848);
   flowt.push_back(4888.1856);
   flowt.push_back(55.2);
   flowt.push_back(64.888);
   flowt.push_back(.255);
   flowt.transverse_node();
   
   cout << endl;
   
   linked_list<string> strang; // string variable
   //prints out strings
   cout << "The following are of 'string' type:" << endl;
   strang.push_back("hello");
   strang.push_back("BlehBleh");
   strang.push_back("set");
   strang.push_back("huttt");
   strang.push_back("OMAHAAAH!");
   strang.transverse_node();
   
   cout << endl;
}