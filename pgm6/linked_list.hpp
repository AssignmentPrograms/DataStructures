#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <iostream>
#include "node.hpp"
#include "iterator.hpp"
using namespace std;


template<class Type>
class linked_list
{
   Node<Type> *head, *tail;
   
public:
   linked_list();
   //void inser   t_node(void)
   //void insert_node(Type data1);
   bool transverse_node();
   void push_back(Type data1);
   Node<Type> *begin();
   Node<Type> *end();
};

template<class Type>
linked_list<Type>::linked_list()
{
   head = tail = NULL;
}


template<class Type>
void linked_list<Type>::push_back(Type data1)
{
   Node<Type> *node_ptr = new Node<Type>(data1);
   
   if(head == NULL)
   {
      tail = head = node_ptr;
   }
   else
   {
      tail->set_link(node_ptr);
      tail = node_ptr;
   }
}

template<class Type>
Node<Type>* linked_list<Type>::begin()
{
   return head;
}
   
   
template<class Type>
Node<Type>* linked_list<Type>::end()
{
   return tail;
}


template<class Type>
bool linked_list<Type>::transverse_node()
{
   if(head == NULL)
   {
      return false;
   }
   
   else
   {
      Iterator<Type> it;
      it.link(head, tail);
      do
      {
	 it.get()->process_data();
      }while( it++ );
   }
   return true;
}




#endif