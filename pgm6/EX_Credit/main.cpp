/******************************************************************************
 
 NOTE TO GRADER:
    THIS IS THE EXTRA CREDIT EXAMPLE, I HAVE INCLUDED OUTPUT FROM MY PROGRAM
    AS WELL. 
 
 
 
   Brandon Jones
   N534H699

       Program #5
   
   Description of problem:
      We wish to use templates to allow for a wide range of data types with the
      same class in use, as opposed to writing the same class multiple times
      we also wish to traverse a list with the use of iterating through the
      list.
   


*/


#include <iostream>
#include "linked_list.hpp"
#include <string>
using namespace std;

// Node class
// This class can be changed to reflect the data that you want to use.


int main (void)
{
    // Create list.  List will be empty at this point.
    Linked_list<double> list;
    
    // Insert nodes
    list.insert_node(23.3);
    list.insert_node (10.1);
    list.insert_node (25);
    list.insert_node (17);
    list.insert_node (27);
    list.insert_node (9.3);
    list.insert_node (9.3);
    list.traverse(); 
    cout << "\n Delete nodes 25 and 9.3 \n";
    list.delete_node (25);
    list.delete_node (9.3);
    list.traverse();
    cout << "\n Retrieved data: " << list.retrieve (17);   
    cout << endl;

// Create list.  List will be empty at this point.
    Linked_list<int> listint;
    
    // Insert nodes
    listint.insert_node(9);
    listint.insert_node (7);
    listint.insert_node (13);
    listint.insert_node (1);
    listint.insert_node (42);
    listint.insert_node (2);
    listint.insert_node (72);
    listint.traverse(); 
    cout << "\n Delete nodes 25 and 9.3 \n";
    listint.delete_node (13);
    listint.delete_node (42);
    listint.traverse();
    cout << "\n Retrieved data: " << listint.retrieve (7);   
    cout << endl;
    //system("Pause");
    
    
    Linked_list<int> ent; // integer holder
   //prints out integers
   cout << "The following are of 'int' type:" << endl;
   ent.insert_node(8);
   ent.insert_node(2);
   ent.insert_node(6);
   ent.insert_node(8);
   ent.insert_node(15);
   ent.traverse();
   
   cout << endl;
   
   
   Linked_list<double> dawble; // double holder
   //prints out doubles
   cout << "The following are of 'double' type:" << endl;
   dawble.insert_node(12.4);
   dawble.insert_node(14.4);
   dawble.insert_node(13.9);
   dawble.insert_node(2.5);
   dawble.insert_node(1452224.566);
   dawble.traverse();
   
   cout << endl;
   
   Linked_list<long> lawng; // long variable
   //prints out longs
   cout << "The following are of 'long' type:" << endl;
   lawng.insert_node(-2151566);
   lawng.insert_node(186549698);
   lawng.insert_node(995);
   lawng.insert_node(4255);
   lawng.insert_node(649);
   lawng.traverse();
   
   cout << endl;
   
   Linked_list<char> chawr; // char variable
   //prints out chars
   cout << "The following are of 'char' type:" << endl;
   chawr.insert_node('a');
   chawr.insert_node('B');
   chawr.insert_node('c');
   chawr.insert_node('5');
   chawr.insert_node('/');
   chawr.traverse();
   
   cout << endl;
   
   Linked_list<float> flowt; // float variable
   //prints out floats
   cout << "The following are of 'float' type:" << endl;
   flowt.insert_node(4548.46848);
   flowt.insert_node(4888.1856);
   flowt.insert_node(55.2);
   flowt.insert_node(64.888);
   flowt.insert_node(.255);
   flowt.traverse();
   
   cout << endl;
   
   Linked_list<string> strang; // string variable
   //prints out strings
   cout << "The following are of 'string' type:" << endl;
   strang.insert_node("hello");
   strang.insert_node("BlehBleh");
   strang.insert_node("set");
   strang.insert_node("huttt");
   strang.insert_node("OMAHAAAH!");
   strang.traverse();
   
   cout << endl;
    
    
    
    
}
