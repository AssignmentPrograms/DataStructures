/******************************************************************************
 
   Brandon Jones
   N534H699

       Program #5
   
   Description of problem:
      We wish to use templates to allow for a wide range of data types with the
      same class in use, as opposed to writing the same class multiple times
      we also wish to traverse a list with the use of iterating through the
      list.
     
   Psudo Code
     Class Name: Node
     Data:       data1 - the variable to store the data of type (WHATEVER)...
		 *link - point that connects one 
		    node to another (points to the next node)
     Operation Functions:
                 Node() - default constructor, setting book, author and link
                    to NULL
                 Node(Type data1) - is the type we wish, but will construct our
                    list with the inserted data and make it the first.
                 set_link(Node<Type>*ptr) - this sets the link to be the 
                    node pointer we create to connect two nodes
                 Node *get_link() - this is to return the current link
                 process_data() - this is responsible for outputting the
                    information the user has entered within the node


*/


#ifndef NODE_H
#define NODE_H
#include <iostream>
using namespace std;

template <class Type>
class Node
{
   Type data;
   Node<Type> *link;

 public:
   Node ();
   Node (Type data);
   void set_link(Node<Type> *ptr)
   {
        link = ptr;
   }
   Node<Type> *get_link()
   {
        return link;
   }
   int compare_data (Type d);
   Type get_data() { return data; }
   void process_data(void);
};

// Default constructor
template <class Type>
Node<Type>::Node()
{
   data = 0;
   link = NULL;
}

// Initialize the Node with the given data.
template <class Type>
Node<Type>::Node (Type d)
{
   data = d;
   link = NULL;          
}

// Check the class data against the data passed to the routine.
// Return 1 if data is equal
// Return 0 if the data passed is less than the class data
// Return -1 if the data passes is greater than the class data.
template <class Type>
int Node<Type>::compare_data (Type d)
{
   if (d == data)
      return 1;
   else if (d < data)
      return 0;
   else 
      return -1;
}

// The process_data in this case simply prints it to the screen.
// It could do anything.  It could modify the data if desired.
template <class Type>
void Node<Type>::process_data(void)
{
   cout << "\n Data: " << data;
}

#endif