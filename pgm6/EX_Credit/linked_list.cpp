
// Linked list class
template <class Type>
class Linked_list
{
   // Keep track of how many are in list.
   int count;
   Node<Type> *head, *tail;
 public:
   Linked_list();
   bool insert_node(Type data); 
   bool delete_node (Type data);
   void traverse(); 
   Type retrieve(Type key);   
};

// Create the linked list.
template <class Type>
Linked_list<Type>::Linked_list()
{
   count = 0;
   head = tail = NULL;
}

// Insert a node in the list.
template <class Type>
bool Linked_list<Type>::insert_node(Type data)
{
   // Create a new node with a NULL ptr.
   Node<Type> *node_ptr = new Node<Type>(data);
   Node<Type> *prev, *curr;
   
   curr = prev = head;
   // If there are no entries in the linked list,
   // add it to the head.
   if (head == NULL)
   {
      tail = head = node_ptr;
      count ++;
   }
   // If the data is greater than the data at the tail,
   // add the data at the end.
   else if (tail->compare_data(data)==-1)
   {
      tail->set_link(node_ptr);
      tail = node_ptr;
      count ++;
   }
   else 
   {
      // Otherwise, find where the data fits
      // Need the previous node address and the next node address
      // Doesn't work if data is equal.
      while (curr->compare_data(data)==-1)
      {
         prev = curr;
         curr = curr->get_link();
      }
      // If the data is equal, don't insert it.
      if (curr->compare_data (data) == 1)
         return false;                 
      // The new node link will be set to the next position
      node_ptr->set_link(curr);
      // If the data that head points to is smaller than the inserted data,
      // then the link of head pointer is modified
      if (head->compare_data (data)==-1)
         prev->set_link(node_ptr);
      else
         // Otherwise, the head pointer should point to the new node 
         head = node_ptr;
      // Keep track of the number of items in linked list.
      count ++;
   }
   return true;
}

// Remove a node from the list.
template <class Type>
bool Linked_list<Type>::delete_node (Type data)
{
   Node<Type> *curr, *prev;
   
   // Start at the head pointer.
   curr = prev = head;
   
   // Go until the end of the list or the data is found
   while (curr != NULL && curr->compare_data (data) != 1)
   {
      prev = curr;
      curr = curr->get_link();
   }
   // If the node is not found, then return out of the routine
   // letting the calling function know the node was not found.
   if (curr == NULL)
      return 0;     // Failed to find node
   else
   {
      // If the data is in the one pointed to by head,
      // then delete it and modify the head pointer.
      if (curr == head)
      {  
         head = curr->get_link();
         delete curr;
      }
      // Otherwise set the link of the next one
      else 
      {
         prev->set_link(curr->get_link());
         delete curr;
      }
      count --;
   }   
}

// Go through the entire list.  Call the node routine to process the data.
template <class Type>
void Linked_list<Type>::traverse()
{
   Node<Type> *temp;
   temp = head;
   while (temp != NULL)
   {
      temp->process_data();
      temp = temp->get_link();
   }     
}

// Retrieval is usually part of a linked list.  
// Included here even though it simply returns the data
// that it is looking for.
template <class Type>
Type Linked_list<Type>::retrieve(Type key)
{
   Node<Type> *temp;
   temp = head;
   // Keep looping until not at the end of the list or
   // the data is equal.
   while (temp != NULL && temp->compare_data(key) != 1)
   {
      temp= temp->get_link();
   }
   return temp->get_data();
}