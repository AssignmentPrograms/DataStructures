/****************************************************************************** 
   Brandon Jones
   N534H699

       Program #4
   
  Description of problem:
     We wish to use templates to allow for a wide range of data types with the
      same class in use, as opposed to writing the same class multiple times
      we also wish to traverse a list with the use of iterating through the
      list.
     
      
  Psudo Code
     Class Name: Iterator
     Data:       *pos - points to a node (position of where we are)
		 *end - points to the end of a list (Tail)
     Operation Functions:
                 Iterator - default constructor
                 link - sets pos and end to different nodes to be used to 
                   increase through our list.
                 operator++ - overloading the ++ operator to increment through
                   our list, parameter = dummy variable
                 *get - returns the pos we are looking at.


*/


#ifndef ITERATOR_H
#define ITERATOR_H
#include "node.hpp"
#include <iostream>
using namespace std;

template<class Type>
class Iterator
{
   //stores the current position node
   Node<Type> *pos;
   //stores the tail node to be used to compare if we are at end of list
   Node<Type> *end;
   
public:
   //default constructor
   Iterator();
   //link to set pos and end to new nodes to be later used.
   void link(Node<Type> *newpos, Node<Type> *newend);
   //overload the ++ operator to be able to cycle through 
   // list using the ++ operator only (DUMMY VARiABLE needed)
   bool operator++(int temp);
   //returns the pos(node) we are looking at
   Node<Type> *get();   
};


template<class Type>
Iterator<Type>::Iterator()
{
   pos = end = NULL;
}

template<class Type>
void Iterator<Type>::link(Node<Type> *newpos, Node<Type> *newend)
{
   pos = newpos;
   end = newend;
}

template<class Type>
bool Iterator<Type>::operator++(int temp)
{
   //if we are at the end ofthe list we do not wish to increment
   if(pos != end)
   {
      //if we are not at the end of the list we wish to increment
      // we do so by getting the link of the current pos (next node)
      pos = pos->get_link();
      //returns true for a do-while loop in linked_list
      //we can increment
      return true;
   }
   else
   {
      //we can not increment
      return false;
   }
   return true;
}

template<class Type>
Node<Type>* Iterator<Type>::get()
{
   return pos;
}


#endif