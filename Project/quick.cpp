/******************************************************************************

   Brandon Jones - N534H699
   Tyler Hull - P622S636

       PROJECT
   
   Description of problem:
      We wish to create arrays of sizes 100, 500, 1000, and use sorts to 
	 properly organize data based off the key correlating to the data
	    and compare to other data in the array to create an ascending
	       list of elements. to be printed to a file and/or screen.
		  utilizes the sort time for each sort, and calculated
		     calculations are on sheet of paper, or can be accessed
			from this program, under debug mode, and
			   printofile = false
	       
      
   this is the quick sort code
   sorts a given array, based off length, and orders in ascending order

*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>


using namespace std;



int QuickSort(Array a[], int left, int right)
{
    //tracks sort time iterations
    int sorttime = 0;
   
    int i = left; //index value
    int j = right; //index value
    Array temp; // temp swap variable
    int pivot = (left+right)/2; // init pivot to median index
    
    //determine median and set pivot
    //pivot should end up at first position
    //and be the middle value of left,right, and median
    if (a[i].key > a[pivot].key)
    {
       temp = a[i];
       a[i] = a[pivot];
       a[pivot] = temp;
    }
    if (a[i].key > a[j].key)
    {
       temp = a[i];
       a[i] = a[j];
       a[j] = temp;
    }
    if (a[pivot].key > a[j].key)
    {
       temp = a[pivot];
       a[pivot] = a[j];
       a[j] = temp;
    }
    temp = a[i];
    a[i] = a[pivot];
    a[pivot] = temp;
    
    //set pivot to left and left to pivot plus 1
    pivot = left;
    i = left + 1;
    
    // loop until left passes right
    while(i <= j)
    {
        //increment left until key value greater than pivot found
        while(a[i].key < a[pivot].key)
        {
           i++;
           sorttime++;
        }
        //decrement right until key value less than pivot found
        while(a[j].key > a[pivot].key)
        {
           j--;
           sorttime++;	   
        }
        //swap element of left and right unless indices have passed
        if(i <= j)
        {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }
        sorttime++;
    }
    
    // place pivot in correct location in list
    temp = a[i-1];
    a[i-1] = a[left];
    a[left] = temp;
    
    // call quicksort for left partition if needed
    if(left < j)
    {
        sorttime += QuickSort(a, left, j);
    }
    // call quicksort for right partition if needed
    if(i < right)
    {
        sorttime += QuickSort(a, i, right);
    }
    
    //return loop iterations
    return sorttime;
}
