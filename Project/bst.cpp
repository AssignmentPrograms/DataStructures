/******************************************************************************

   Brandon Jones - N534H699
   Tyler Hull - P622S636

       PROJECT
   
   Description of problem:
      We wish to create arrays of sizes 100, 500, 1000, and use sorts to 
	 properly organize data based off the key correlating to the data
	    and compare to other data in the array to create an ascending
	       list of elements. to be printed to a file and/or screen.
		  utilizes the sort time for each sort, and calculated
		     calculations are on sheet of paper, or can be accessed
			from this program, under debug mode, and
			   printofile = false
	       
      
   this is the BST (BINARY SEARCH TREE) sort code
   sorts a given array, based off length, and orders in ascending order
   
   utilizes the class of BST to do many operations in a single group  


*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>


using namespace std;

//THIS IS A BINARY SEARCH TREE
class BST
{
    //Structure for our node we use
    struct Node
    {
        int key;
        double data;
        Node* left;
        Node* right;
    };
    //count to track interations
    int count;
    //creates our root
    Node* root;
    
    //Adds a leaf to the tree
    int AddLeafPrivate(int key, Node* ptr, double data)
    {
        int sorttime = 0;
        if(root == NULL)
        {
            root = CreateLeaf(key, data);
        }
        else if(key < ptr->key)
        {
            if(ptr->left != NULL)
            {
                sorttime += AddLeafPrivate(key, ptr->left, data);
            }
            else
            {
                ptr->left = CreateLeaf(key, data);
            }
        }
        else if(key >= ptr->key)
        {
            if(ptr->right != NULL)
            {
                sorttime += AddLeafPrivate(key, ptr->right, data);
            }
            else
            {
                ptr->right = CreateLeaf(key, data);
            }
        }
        else
        {
            count++;
        }
        
        
        
	return sorttime + 1;
    }
    
    //prints the tree in numerical order
    void PrintInOrderPrivate(Node* ptr)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderPrivate(ptr->left);
            }
            cout << "Key: " << ptr-> key << "     Data: " << ptr->data << endl;
            if(ptr->right != NULL)
            {
                PrintInOrderPrivate(ptr->right);
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    
    //prints in numerical order (USED TO TRACK ITERATIONS)
    int PrintInOrderNEWPRIVATE(Node* ptr)
    {
        int sorttime = 0;
	
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                sorttime += PrintInOrderNEWPRIVATE(ptr->left);
            }
            cout << "Key: " << ptr-> key << "     Data: " << ptr->data << endl;
            if(ptr->right != NULL)
            {
                sorttime += PrintInOrderNEWPRIVATE(ptr->right);
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
        return sorttime + 1;
    }
    
    //PRINTS IN ORDER TO FILE
    void PrintInOrderToFilePrivate(Node* ptr, ofstream& OutputFile)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderToFilePrivate(ptr->left, OutputFile);
            }
            OutputFile << "Key: " << ptr-> key << "     Data: " << ptr->data << endl;
            if(ptr->right != NULL)
            {
                PrintInOrderToFilePrivate(ptr->right, OutputFile);
            }
        }
        else
        {
            //cout << "The tree is empty\n";
        }
    }
    
    //Returns Node if we wish to access one
    Node* ReturnNodePrivate(int key, Node* ptr, double data)
    {
        if(ptr != NULL)
        {
            if(ptr->key == key)
            {
                return ptr;
            }
            else
            {
                if(key < ptr->key)
                {
                    return ReturnNodePrivate(key, ptr->left, data);
                }
                else//Greater than
                {
                    return ReturnNodePrivate(key, ptr->right, data);
                }
            }
        }
            return NULL;
        
    }
    
    //Finds the smallest value in node
    int FindSmallestPrivate(Node* ptr)
    {
        if(root == NULL)
        {
            cout << "The tree is empty\n";
            return -1000; // needs to adjust if there are negative numbers
        }
        else
        {
            if(ptr->left != NULL)
            {
                return FindSmallestPrivate(ptr->left);
            }
            //no need to look at right child (would always be bigger than
            else
            {
                return ptr->key;
            }
        }
    }
    
    //Removes a node we desired
    void RemoveNodePrivate(int key, Node* parent, bool main)
    {
        if(root != NULL)
        {
            if(root->key == key)
            {
                RemoveRootMatch();
            }
            else
            {
                if(key < parent->key && parent->left != NULL)
                {
                    if(parent->left->key == key)
		    {
                       RemoveMatch(parent, parent->left, main);
		    }
		    else
		    {
                       RemoveNodePrivate(key, parent->left, main);
		    }
                }
                else if(key > parent->key && parent->right != NULL)
                {
                    if(parent->right->key == key)
		    {
                       RemoveMatch(parent, parent->right, main);
		    }
		    else
		    {
                       RemoveNodePrivate(key, parent->right, main);
		    }
                }
                else
                {
                    cout << "The key " << key << " was not found in the tree\n";
                }
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    
    //Removes the root if we find it
    void RemoveRootMatch()
    {
        if(root != NULL)
        {
            Node* delPtr = new Node;
            delPtr->key = root->key;
            int rootKey = root->key;
            int smallestInRightSubtree;

            // Case 0 - 0 children
            if(root->left == NULL && root->right == NULL)
            {
                root = NULL;
                delete delPtr;
            }

            // Case 1 - 1 child
            else if(root->left == NULL && root->right != NULL)
            {
                root = root->right;
                delPtr->right = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }
            else if(root->left != NULL && root->right == NULL)
            {
                root = root->left;
                delPtr->left = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }

            // Case 2 - 2 children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(root->right);
                RemoveNodePrivate(smallestInRightSubtree, root, true);
                root->key = smallestInRightSubtree;
                cout << "The root key containing key " << rootKey <<
                        " was overwritten with key " << root->key << endl;
            }
        }
        else
        {
            cout << "Can not remove root. The tree is empty\n";
        }
    }
    
    //removes a selected node
    void RemoveMatch(Node* parent, Node* match, bool main)
    {
        int smallest;
        if(root != NULL)
        {
            Node* delPtr = new Node;
            int matchKey = match->key;
            int smallestInRightSubtree;

            // Case 0 - 0 Children
            if(match->left == NULL && match->right == NULL)
            {
                delPtr->key = match->key;
                if(main)
                {
                    if(parent->left != root->left)
                    {
                        parent->left = NULL;
                    }
                    else
                    {
                        parent->right = NULL;
                    }
                }
                else
                {
                    delPtr->key = match->key;
                    if(parent->left->key == match->key)
                    {
                        if(match->left == NULL && match->right == NULL)
                        {
                            parent->left = NULL;
                        }
                        else
                        {
                            smallest = FindSmallestPrivate(parent->left);
                            if(match->key == smallest)
                            {
                                parent->left = match->right;
                            }
                            else
                            {
                                parent->key = smallest;
                                RemoveNodePrivate(smallest, parent, true);
                                cout << endl << "HEREEEEE" << endl;
                            }
                        }
                    }
                    else if(parent->right->key == match->key)
                    {
                        if(match->left == NULL && match->right == NULL)
                        {
                            parent->right = NULL;
                        }
                        else
                        {
                            smallest = FindSmallestPrivate(parent->right);
                            if(match->key == smallest)
                            {
                                parent->left = match->right;
                            }
                            else
                            {
                                parent->key = smallest;
                                RemoveNodePrivate(smallest, parent, true);
                                cout << endl << "HEREEEEE" << endl;
                            }
                        }
                    }
                    //main == true ? parent->left = NULL : parent->right = NULL;
                }
                //left == true ? parent->left = NULL : parent->right = NULL;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 1 - 1 Child
            else if(match->left == NULL && match->right != NULL)
            {
                //left == true ? parent->right = match->right : parent->left = match->right;
                if(parent->left != root->left)
                {
                   parent->left = match->right;
                }
                else
                {
                parent->right = match->right;
                }
                match->right = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }
            else if(match->left != NULL && match->right == NULL)
            {
                //left == true ? parent->left = match->left : parent->right = match->left;
                if(parent->left != root->left)
                {
                   parent->left = match->left;
                }
                else
                {
                    parent->right = match->left;
                }
                match->left = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 2 - 2 Children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(match->right);
                RemoveNodePrivate(smallestInRightSubtree, match, main);
                match->key = smallestInRightSubtree;
                //cout << smallestInRightSubtree << endl;
            }
        }
        else
        {
            cout << "Can not remove match. The tree is empty\n";
        }
    }
    
    //Removes a subtree if we desire
    void RemoveSubtree(Node* ptr) //post order traversal actually
    {
        if(ptr != NULL)
        {
            if(ptr->left != NULL)
            {
                RemoveSubtree(ptr->left);
            }
            if(ptr->right != NULL)
            {
                RemoveSubtree(ptr->right);
            }
            cout << "Deleting the node containing key " << ptr->key << endl;
            delete ptr;
        }
    }

//public functions
public:
    //default constructor
    BST()
    {
        root = NULL;
    }
    
    //deconstructor
    /*
    ~BST()
    {
        RemoveSubtree(root);
    }
    */
    
    //creates a leaf
    Node* CreateLeaf(int key, double data) //add toprivate
    {
        Node* node_ptr = new Node;
        node_ptr->key = key; // first key is the key inside node, second key is the key being passed in
        node_ptr->data = data;
        node_ptr->left = NULL;
        node_ptr->right = NULL;

        return node_ptr;
    }
    //calls function to add leaf with data and key
    int AddLeaf(int key, double data)
    {
        return AddLeafPrivate(key, root, data);
    }
    //prints in order (Calls function to do so)
    //KEEPS ROOT PRIVATE
    void PrintInOrder()
    {
        PrintInOrderPrivate(root);
    }
    //Prints in order but returns the value we wish to store
    //KEEPS ROOT PRIVATE
    int PrintInOrderNEW()
    {
       return PrintInOrderNEWPRIVATE(root);
    }
    //prints in order to file
    void PrintInOrderToFile(ofstream& OutputFile)
    {
        PrintInOrderToFilePrivate(root, OutputFile);
    }
    //Returns a node we wish to find
    Node* ReturnNode(int key, double data) //helper function //add to private
    {
        return ReturnNodePrivate(key, root, data);
    }
    //returns the key of the root if we wish
    int ReturnRootKey()
    {
        if(root != NULL)
        {
            return root->key;
        }
        return 0;
    }
    //prints children of functions
    void PrintChildren(int key, double data)
    {
        Node* ptr = ReturnNode(key, data);

        if(ptr != NULL)
        {
            cout << "Parent Node = " << ptr->key << endl;
            cout << "NAME: " << ptr->data << endl;

            ptr->left == NULL ?
            cout << "Left Child = NULL\n":  //true
            cout << "Left Child = " << ptr->left->key << endl; //false
            cout << "NAME: " << ptr->data << endl;

            ptr->right == NULL ?
            cout << "Right Child = NULL\n":
            cout << "Right Child = " << ptr->right->key << endl;
            cout << "NAME: " << ptr->data << endl;
        }
        else
        {
            cout << "Key " << key << " is not in the tree\n";
        }
    }
    //finds the smallest - calls finds smallest private
    //KEEPS ROOT PRIVATE
    int FindSmallest()
    {
        return FindSmallestPrivate(root);
    }
    //removes a node - calls the remove node private
    //KEEPS ROOT PRIVATE
    void RemoveNode(int key)
    {
        RemoveNodePrivate(key, root, false);
    }


};