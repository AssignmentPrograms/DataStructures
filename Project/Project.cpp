#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;



class BST
{
    struct Node
    {
        int key;
        double data;
        Node* left;
        Node* right;
    };
    Node* root;
    void AddLeafPrivate(int key, Node* ptr, double data)
    {
        if(root == NULL)
        {
            root = CreateLeaf(key, data);
        }
        else if(data < ptr->data)
        {
            if(ptr->left != NULL)
            {
                AddLeafPrivate(key, ptr->left, data);
            }
            else
            {
                ptr->left = CreateLeaf(key, data);
            }
        }
        else if(data > ptr->data)
        {
            if(ptr->right != NULL)
            {
                AddLeafPrivate(key, ptr->right, data);
            }
            else
            {
                ptr->right = CreateLeaf(key, data);
            }
        }
        else
        {
            //cout << "The key " << key << " has already been added to the tree\n";
        }
    }
    void PrintInOrderPrivate(Node* ptr)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderPrivate(ptr->left);
            }
            cout << ptr->data << " ";
            if(ptr->right != NULL)
            {
                PrintInOrderPrivate(ptr->right);
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    Node* ReturnNodePrivate(int key, Node* ptr, double data)
    {
        if(ptr != NULL)
        {
            if(ptr->key == key)
            {
                return ptr;
            }
            else
            {
                if(key < ptr->key)
                {
                    return ReturnNodePrivate(key, ptr->left, data);
                }
                else//Greater than
                {
                    return ReturnNodePrivate(key, ptr->right, data);
                }
            }
        }
        else
        {
            return NULL;
        }
    }
    int FindSmallestPrivate(Node* ptr)
    {
        if(root == NULL)
        {
            cout << "The tree is empty\n";
            return -1000; // needs to adjust if there are negative numbers
        }
        else
        {
            if(ptr->left != NULL)
            {
                return FindSmallestPrivate(ptr->left);
            }
            //no need to look at right child (would always be bigger than
            else
            {
                return ptr->key;
            }
        }
    }
    void RemoveNodePrivate(int key, Node* parent, bool main)
    {
        if(root != NULL)
        {
            if(root->key == key)
            {
                RemoveRootMatch();
            }
            else
            {
                if(key < parent->key && parent->left != NULL)
                {
                    if(parent->left->key == key)
		    {
                       RemoveMatch(parent, parent->left, main);
		    }
		    else
		    {
                       RemoveNodePrivate(key, parent->left, main);
		    }
                }
                else if(key > parent->key && parent->right != NULL)
                {
                    if(parent->right->key == key)
		    {
                       RemoveMatch(parent, parent->right, main);
		    }
		    else
		    {
                       RemoveNodePrivate(key, parent->right, main);
		    }
                }
                else
                {
                    cout << "The key " << key << " was not found in the tree\n";
                }
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    void RemoveRootMatch()
    {
        if(root != NULL)
        {
            Node* delPtr = new Node;
            delPtr->key = root->key;
            int rootKey = root->key;
            int smallestInRightSubtree;

            // Case 0 - 0 children
            if(root->left == NULL && root->right == NULL)
            {
                root = NULL;
                delete delPtr;
            }

            // Case 1 - 1 child
            else if(root->left == NULL && root->right != NULL)
            {
                root = root->right;
                delPtr->right = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }
            else if(root->left != NULL && root->right == NULL)
            {
                root = root->left;
                delPtr->left = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }

            // Case 2 - 2 children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(root->right);
                RemoveNodePrivate(smallestInRightSubtree, root, true);
                root->key = smallestInRightSubtree;
                cout << "The root key containing key " << rootKey <<
                        " was overwritten with key " << root->key << endl;
            }
        }
        else
        {
            cout << "Can not remove root. The tree is empty\n";
        }
    }
    void RemoveMatch(Node* parent, Node* match, bool main)
    {
        int smallest;
        if(root != NULL)
        {
            Node* delPtr = new Node;
            int matchKey = match->key;
            int smallestInRightSubtree;

            // Case 0 - 0 Children
            if(match->left == NULL && match->right == NULL)
            {
                delPtr->key = match->key;
                if(main)
                {
                    if(parent->left != root->left)
                    {
                        parent->left = NULL;
                    }
                    else
                    {
                        parent->right = NULL;
                    }
                }
                else
                {
                    delPtr->key = match->key;
                    if(parent->left->key == match->key)
                    {
                        if(match->left == NULL && match->right == NULL)
                        {
                            parent->left = NULL;
                        }
                        else
                        {
                            smallest = FindSmallestPrivate(parent->left);
                            if(match->key == smallest)
                            {
                                parent->left = match->right;
                            }
                            else
                            {
                                parent->key = smallest;
                                RemoveNodePrivate(smallest, parent, true);
                                cout << endl << "HEREEEEE" << endl;
                            }
                        }
                    }
                    else if(parent->right->key == match->key)
                    {
                        if(match->left == NULL && match->right == NULL)
                        {
                            parent->right = NULL;
                        }
                        else
                        {
                            smallest = FindSmallestPrivate(parent->right);
                            if(match->key == smallest)
                            {
                                parent->left = match->right;
                            }
                            else
                            {
                                parent->key = smallest;
                                RemoveNodePrivate(smallest, parent, true);
                                cout << endl << "HEREEEEE" << endl;
                            }
                        }
                    }
                    //main == true ? parent->left = NULL : parent->right = NULL;
                }
                //left == true ? parent->left = NULL : parent->right = NULL;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 1 - 1 Child
            else if(match->left == NULL && match->right != NULL)
            {
                //left == true ? parent->right = match->right : parent->left = match->right;
                if(parent->left != root->left)
                {
                   parent->left = match->right;
                }
                else
                {
                parent->right = match->right;
                }
                match->right = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }
            else if(match->left != NULL && match->right == NULL)
            {
                //left == true ? parent->left = match->left : parent->right = match->left;
                if(parent->left != root->left)
                {
                   parent->left = match->left;
                }
                else
                {
                    parent->right = match->left;
                }
                match->left = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 2 - 2 Children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(match->right);
                RemoveNodePrivate(smallestInRightSubtree, match, main);
                match->key = smallestInRightSubtree;
                //cout << smallestInRightSubtree << endl;
            }
        }
        else
        {
            cout << "Can not remove match. The tree is empty\n";
        }
    }
    void RemoveSubtree(Node* ptr) //post order traversal actually
    {
        if(ptr != NULL)
        {
            if(ptr->left != NULL)
            {
                RemoveSubtree(ptr->left);
            }
            if(ptr->right != NULL)
            {
                RemoveSubtree(ptr->right);
            }
            cout << "Deleting the node containing key " << ptr->key << endl;
            delete ptr;
        }
    }

public:
    BST()
    {
        root = NULL;
    }
    /*
    ~BST()
    {
        RemoveSubtree(root);
    }
    */
    Node* CreateLeaf(int key, double data) //add toprivate
    {
        Node* node_ptr = new Node;
        node_ptr->key = key; // first key is the key inside node, second key is the key being passed in
        node_ptr->data = data;
        node_ptr->left = NULL;
        node_ptr->right = NULL;

        return node_ptr;
    }

    void AddLeaf(int key, double data)
    {
        AddLeafPrivate(key, root, data);
    }
    void PrintInOrder()
    {
        PrintInOrderPrivate(root);
    }
    Node* ReturnNode(int key, double data) //helper function //add to private
    {
        ReturnNodePrivate(key, root, data);
    }
    int ReturnRootKey()
    {
        if(root != NULL)
        {
            return root->key;
        }
        else
        {
            //will need to adjust if we have negative numbers
        }
    }
    void PrintChildren(int key, double data)
    {
        Node* ptr = ReturnNode(key, data);

        if(ptr != NULL)
        {
            cout << "Parent Node = " << ptr->key << endl;
            cout << "NAME: " << ptr->data << endl;

            ptr->left == NULL ?
            cout << "Left Child = NULL\n":  //true
            cout << "Left Child = " << ptr->left->key << endl; //false
            cout << "NAME: " << ptr->data << endl;

            ptr->right == NULL ?
            cout << "Right Child = NULL\n":
            cout << "Right Child = " << ptr->right->key << endl;
            cout << "NAME: " << ptr->data << endl;
        }
        else
        {
            cout << "Key " << key << " is not in the tree\n";
        }
    }
    int FindSmallest()
    {
        FindSmallestPrivate(root);
    }
    void RemoveNode(int key)
    {
        RemoveNodePrivate(key, root, false);
    }


};


void SelectionSort(int a[], int length)
{
    int smallest;
    int walker;
    int minIndex;

    for(int curr = 0; curr < length -1; curr++)
    {
        smallest = a[curr];

        for(walker = curr+1; walker < length; walker++)
        {
            if(a[walker] < smallest)
            {
                smallest = a[walker];
                minIndex = walker;
            }
        }
        a[minIndex] = a[curr];
        a[curr] = smallest;
    }
}



void InsertSort(int a[], int length)
{

    for(int curr = 1; curr < length ; curr++)
    {
        int hold = a[curr] , walker = curr - 1;

        while(walker >= 0 && hold < a[walker])
        {
            a[walker + 1] = a[walker]; walker--;
        }

        a[walker+1] = hold;
    }
}

/*
void BubbleSort(int a[], int length)
{
    int last = a[length - 1];
    int curr = 0;
    int walker;
    bool sorted = false;

    while(curr <= last && !sorted)
    {
        walker = a[last];
        sorted = true;
        while(curr() > a[curr])
        {
            if(walker < )
            {
                sorted = false;
            //exchange list, walker, ...?
            }
        }

    }
}
*/

void BubbleSort(int a[], int length)
{
    int temp;
    for(int curr = 0; curr < length; curr++)
    {
        for(int walker = 0; walker < length; walker++)
        {
            if(a[curr] < a[walker])
            {
                temp = a[curr];
                a[curr] = a[walker];
                a[walker] = temp;
            }
        }
    }
}

/*
void QuickSort(int a[], int length)
{
    left = a[0];
    right = a[length - 1];

    if((right-left) > minsize)
    {

    }
}
*/


void QuickSort(int a[], int left, int right)
{
    int i = left;
    int j = right;
    int temp;
    int pivot = a[(left+right)/2];
    while(i <= j)
    {
        while(a[i] < pivot)
            i++;
        while(a[j] > pivot)
            j--;
        if(i <= j)
        {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j++;
        }
    }
    if(left < j)
    {
        QuickSort(a, 0, j);
    }
    if(i < right)
    {
        QuickSort(a, i, right);
    }


}



void PrintArray(int a[], int length)
{
    for(int i = 0; i < length; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

void PrintArrayToFile(int a[], int length, ofstream& OutputFile)
{
    int count = 1;
    for(int i = 0; i < length; i++)
    {
        OutputFile << a[i] << "     ";
        count ++;
        if(count%21 == 20)
        {
            OutputFile << endl;
        }
    }
    OutputFile << endl;
}


int main()
{
    const int small = 100;
    const int medium = 500;
    const int large = 1000;
    int size = large;
    int a[small];
    int b[medium];
    int c[large];
    srand(unsigned(time(0)));
    bool BST_MODE = true;

    BST Small;
    BST Medium;
    BST Large;

    bool small_tree = true;
    bool medium_tree = true;
    bool large_tree = true;

    for(int i = 0; i < size; i++)
    {
        a[i] = (rand() % 5000) + 1;
        b[i] = (rand() % 5000) + 1;
        c[i] = (rand() % 5000) + 1;
    }

    cout << "Before sorting" << endl;
    PrintArray(a, small);
    cout << endl;


    if(BST_MODE)
    {
        cout << endl << "Printing from tree" << endl;
        if(small_tree)
        {
            for(int i = 0; i < small; i++)
            {
                Small.AddLeaf(i, (rand() % 5000) + 1);
            }
            cout << endl << "Small data set printed from tree" << endl;
            Small.PrintInOrder();
            cout << endl;
        }
        if(medium_tree)
        {
            for(int i = 0; i < medium; i++)
            {
                Medium.AddLeaf(i, (rand() % 5000) + 1);
            }
            cout << endl << "Medium data set printed from tree" << endl;
            Medium.PrintInOrder();
            cout << endl;
        }
        if(large_tree)
        {
            for(int i = 0; i < large; i++)
            {
                Large.AddLeaf(i, (rand() % 5000) + 1);
            }
            cout << endl << "Large data set printed from tree" << endl;
            Large.PrintInOrder();
            cout << endl;
        }
    }


    cout << endl;

    bool Selection_Sort = true;
    bool Insert_Sort = true;
    bool Bubble_Sort = true;
    bool Quick_Sort = true; // Bug -- (bigger than like 10) -> crash
    bool toFile = true;

    string file;

    //List 100;


    if(Selection_Sort)
    {
        if(toFile)
        {
            SelectionSort(a, small);
            SelectionSort(b, medium);
            SelectionSort(c, large);
            ofstream OutputFile;
            OutputFile.open("selsxxx.dat");
            file = "selsxxx.dat";
            if(OutputFile.fail())
            {
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl;
            cout << endl;
            OutputFile << endl << "After sorting with Selection Sort"  << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(a, small, OutputFile);
            OutputFile << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(b, medium, OutputFile);
            OutputFile << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(c, large, OutputFile);
            OutputFile << endl;
        }
        else
        {
            cout << endl << "After sorting with Selection Sort" << endl;
            SelectionSort(a, small);
            PrintArray(a, small);
        }

    }

    if(Insert_Sort)
    {
        if(toFile)
        {
            InsertSort(a, small);
            InsertSort(b, medium);
            InsertSort(c, large);
            ofstream OutputFile;
            OutputFile.open("sinsxxx.dat");
            file = "sinsxxx.dat";
            if(OutputFile.fail())
            {
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            cout << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl;
            cout << endl;
            OutputFile << endl << "After sorting with Insert Sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(a, small, OutputFile);
            OutputFile << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(b, medium, OutputFile);
            OutputFile << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(c, large, OutputFile);
            OutputFile << endl;
        }
        else
        {
            cout << endl << "After sorting with Insert Sort" << endl;
            InsertSort(a, small);
            PrintArray(a, small);
        }

    }

    if(Bubble_Sort)
    {
        if(toFile)
        {
            BubbleSort(a, small);
            BubbleSort(b, medium);
            BubbleSort(c, large);
            ofstream OutputFile;
            OutputFile.open("bubxxx.dat");
            file = "bubxxx.dat";
            if(OutputFile.fail())
            {
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            cout << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl;
            cout << endl;
            OutputFile << endl << "After sorting with Bubble Sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(a, small, OutputFile);
            OutputFile << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(b, medium, OutputFile);
            OutputFile << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(c, large, OutputFile);
            OutputFile << endl;
        }
        else
        {
            cout << endl << "After sorting with Bubble Sort" << endl;
            BubbleSort(a, small);
            PrintArray(a, small);
        }
    }


    /*
    To Do:
            Fix QuickSort Bug -> right side value (pass through)
    */
    if(Quick_Sort)
    {
        if(toFile)
        {
            QuickSort(a, 0, small);
            QuickSort(b, 0, medium);
            QuickSort(c, 0, large);
            ofstream OutputFile;
            OutputFile.open("quickxxx.dat");
            file = "quickxxx.dat";
            if(OutputFile.fail())
            {
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            cout << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl;
            cout << endl;
            OutputFile << endl << "After sorting with Quick Sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(a, small, OutputFile);
            OutputFile << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(b, medium, OutputFile);
            OutputFile << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(c, large, OutputFile);
            OutputFile << endl;
        }
        else
        {
            cout << endl << "After sorting with Quick Sort" << endl;
            QuickSort(a, 0, 50);
            PrintArray(a, small);
        }

    }

    cout << "Big - O calculated value: " << endl; // ADD CALCULATION
    cout << "Selection sort time: " << endl; // ADD CALCULATION
    cout << "Straight insertion sort time: " << endl; // ADD CALCULATION
    cout << "Bubble sort time: " << endl; // ADD CALCULATION
    cout << "Quick sort time: " << endl; // ADD CALCULATION
    cout << "Binary search tree time: " << endl; // ADD CALCULATION


    return 0;
}
