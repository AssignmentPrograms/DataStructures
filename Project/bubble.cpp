/******************************************************************************

   Brandon Jones - N534H699
   Tyler Hull - P622S636

       PROJECT
   
   Description of problem:
      We wish to create arrays of sizes 100, 500, 1000, and use sorts to 
	 properly organize data based off the key correlating to the data
	    and compare to other data in the array to create an ascending
	       list of elements. to be printed to a file and/or screen.
		  utilizes the sort time for each sort, and calculated
		     calculations are on sheet of paper, or can be accessed
			from this program, under debug mode, and
			   printofile = false
	       
      
   this is the bubble sort code
   sorts a given array, based off length, and orders in ascending order

*/

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>


using namespace std;



int BubbleSort(Array a[], int length)
{
    //tracks sort time iterations
    int sorttime = 0;
   
    Array temp; // temp storage for swaps
    
    // iterate through entire array and will bubble largest to top
    // runs array length times
    for(int curr = 0; curr < length; curr++)
    {
        // inner loop to bubble through entire array bubbling largest
        // to top
        for(int walker = 0; walker < length; walker++)
        {
	    // swap if current < walker
            if(a[curr].key < a[walker].key)
            {
                temp = a[curr];
                a[curr] = a[walker];
                a[walker] = temp;
            }
            sorttime++;
        }
    }
    return sorttime ;
}