/******************************************************************************

   Brandon Jones - N534H699
   Tyler Hull - P622S636

       PROJECT
   
   Description of problem:
      We wish to create arrays of sizes 100, 500, 1000, and use sorts to 
	 properly organize data based off the key correlating to the data
	    and compare to other data in the array to create an ascending
	       list of elements. to be printed to a file and/or screen.
		  utilizes the sort time for each sort, and calculated
		     calculations are on sheet of paper, or can be accessed
			from this program, under debug mode, and
			   printofile = false
	       
      
   
   this is the insertion sort code
   sorts a given array, based off length, and orders in ascending order

*/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>


using namespace std;



int InsertSort(Array a[], int length)
{
    //tracks sort time iterations
    int sorttime = 1;
    
    // selects current hold and walks it back through array
    // to correct location
    for(int curr = 1; curr < length ; curr++)
    {
        Array hold = a[curr];
        int walker = curr - 1;

	//iterates through the list, comparing data in our hold key
        while(walker >= 0 && hold.key < a[walker].key)
        {
	    //adjsuts walker, and decrements walker
            a[walker + 1] = a[walker]; walker--;
	    sorttime++;
        }
        //
        a[walker+1] = hold;
	
	
    }
    
    return sorttime;
}