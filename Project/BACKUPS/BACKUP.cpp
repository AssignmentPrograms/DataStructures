#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

using namespace std;

/*
class Array
{
   
   int key;
   float data;
   
public:
   
   Array()
   {
      
   }
   
};
*/

struct Array
{
   int key;
   float data;
};

class BST
{
    struct Node
    {
        int key;
        double data;
        Node* left;
        Node* right;
    };
    int count;
    Node* root;
    
    int AddLeafPrivate(int key, Node* ptr, double data, int sorttime)
    {
        if(root == NULL)
        {
            root = CreateLeaf(key, data);
	    sorttime++;
        }
        else if(key < ptr->key)
        {
            if(ptr->left != NULL)
            {
                sorttime = AddLeafPrivate(key, ptr->left, data, sorttime);
            }
            else
            {
                ptr->left = CreateLeaf(key, data);
		sorttime++;
            }
        }
        else if(key > ptr->key)
        {
            if(ptr->right != NULL)
            {
                sorttime = AddLeafPrivate(key, ptr->right, data, sorttime);
            }
            else
            {
                ptr->right = CreateLeaf(key, data);
		sorttime++;
            }
        }
        else
        {
            count++;
        }
        
        
        
	return sorttime;
    }
    
    void PrintInOrderPrivate(Node* ptr)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderPrivate(ptr->left);
            }
            cout << "Key: " << ptr-> key << "     Data: " << ptr->data << endl;
            if(ptr->right != NULL)
            {
                PrintInOrderPrivate(ptr->right);
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    
    void PrintInOrderToFilePrivate(Node* ptr, ofstream& OutputFile)
    {
        if(root != NULL)
        {
            if(ptr->left != NULL)
            {
                PrintInOrderToFilePrivate(ptr->left, OutputFile);
            }
            OutputFile << "Key: " << ptr-> key << "     Data: " << ptr->data << endl;
            if(ptr->right != NULL)
            {
                PrintInOrderToFilePrivate(ptr->right, OutputFile);
            }
        }
        else
        {
            //cout << "The tree is empty\n";
        }
    }
    Node* ReturnNodePrivate(int key, Node* ptr, double data)
    {
        if(ptr != NULL)
        {
            if(ptr->key == key)
            {
                return ptr;
            }
            else
            {
                if(key < ptr->key)
                {
                    return ReturnNodePrivate(key, ptr->left, data);
                }
                else//Greater than
                {
                    return ReturnNodePrivate(key, ptr->right, data);
                }
            }
        }
        else
        {
            return NULL;
        }
    }
    int FindSmallestPrivate(Node* ptr)
    {
        if(root == NULL)
        {
            cout << "The tree is empty\n";
            return -1000; // needs to adjust if there are negative numbers
        }
        else
        {
            if(ptr->left != NULL)
            {
                return FindSmallestPrivate(ptr->left);
            }
            //no need to look at right child (would always be bigger than
            else
            {
                return ptr->key;
            }
        }
    }
    void RemoveNodePrivate(int key, Node* parent, bool main)
    {
        if(root != NULL)
        {
            if(root->key == key)
            {
                RemoveRootMatch();
            }
            else
            {
                if(key < parent->key && parent->left != NULL)
                {
                    if(parent->left->key == key)
		    {
                       RemoveMatch(parent, parent->left, main);
		    }
		    else
		    {
                       RemoveNodePrivate(key, parent->left, main);
		    }
                }
                else if(key > parent->key && parent->right != NULL)
                {
                    if(parent->right->key == key)
		    {
                       RemoveMatch(parent, parent->right, main);
		    }
		    else
		    {
                       RemoveNodePrivate(key, parent->right, main);
		    }
                }
                else
                {
                    cout << "The key " << key << " was not found in the tree\n";
                }
            }
        }
        else
        {
            cout << "The tree is empty\n";
        }
    }
    void RemoveRootMatch()
    {
        if(root != NULL)
        {
            Node* delPtr = new Node;
            delPtr->key = root->key;
            int rootKey = root->key;
            int smallestInRightSubtree;

            // Case 0 - 0 children
            if(root->left == NULL && root->right == NULL)
            {
                root = NULL;
                delete delPtr;
            }

            // Case 1 - 1 child
            else if(root->left == NULL && root->right != NULL)
            {
                root = root->right;
                delPtr->right = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }
            else if(root->left != NULL && root->right == NULL)
            {
                root = root->left;
                delPtr->left = NULL;
                delete delPtr;
                cout << "The root node with key " << rootKey << " was deleted. " <<
                        "The new root contains key " << root->key << endl;
            }

            // Case 2 - 2 children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(root->right);
                RemoveNodePrivate(smallestInRightSubtree, root, true);
                root->key = smallestInRightSubtree;
                cout << "The root key containing key " << rootKey <<
                        " was overwritten with key " << root->key << endl;
            }
        }
        else
        {
            cout << "Can not remove root. The tree is empty\n";
        }
    }
    void RemoveMatch(Node* parent, Node* match, bool main)
    {
        int smallest;
        if(root != NULL)
        {
            Node* delPtr = new Node;
            int matchKey = match->key;
            int smallestInRightSubtree;

            // Case 0 - 0 Children
            if(match->left == NULL && match->right == NULL)
            {
                delPtr->key = match->key;
                if(main)
                {
                    if(parent->left != root->left)
                    {
                        parent->left = NULL;
                    }
                    else
                    {
                        parent->right = NULL;
                    }
                }
                else
                {
                    delPtr->key = match->key;
                    if(parent->left->key == match->key)
                    {
                        if(match->left == NULL && match->right == NULL)
                        {
                            parent->left = NULL;
                        }
                        else
                        {
                            smallest = FindSmallestPrivate(parent->left);
                            if(match->key == smallest)
                            {
                                parent->left = match->right;
                            }
                            else
                            {
                                parent->key = smallest;
                                RemoveNodePrivate(smallest, parent, true);
                                cout << endl << "HEREEEEE" << endl;
                            }
                        }
                    }
                    else if(parent->right->key == match->key)
                    {
                        if(match->left == NULL && match->right == NULL)
                        {
                            parent->right = NULL;
                        }
                        else
                        {
                            smallest = FindSmallestPrivate(parent->right);
                            if(match->key == smallest)
                            {
                                parent->left = match->right;
                            }
                            else
                            {
                                parent->key = smallest;
                                RemoveNodePrivate(smallest, parent, true);
                                cout << endl << "HEREEEEE" << endl;
                            }
                        }
                    }
                    //main == true ? parent->left = NULL : parent->right = NULL;
                }
                //left == true ? parent->left = NULL : parent->right = NULL;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 1 - 1 Child
            else if(match->left == NULL && match->right != NULL)
            {
                //left == true ? parent->right = match->right : parent->left = match->right;
                if(parent->left != root->left)
                {
                   parent->left = match->right;
                }
                else
                {
                parent->right = match->right;
                }
                match->right = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }
            else if(match->left != NULL && match->right == NULL)
            {
                //left == true ? parent->left = match->left : parent->right = match->left;
                if(parent->left != root->left)
                {
                   parent->left = match->left;
                }
                else
                {
                    parent->right = match->left;
                }
                match->left = NULL;
                delPtr->key = match->key;
                delete delPtr;
                cout << "The node containing key " << matchKey << " was removed\n";
            }

            // Case 2 - 2 Children
            else
            {
                smallestInRightSubtree = FindSmallestPrivate(match->right);
                RemoveNodePrivate(smallestInRightSubtree, match, main);
                match->key = smallestInRightSubtree;
                //cout << smallestInRightSubtree << endl;
            }
        }
        else
        {
            cout << "Can not remove match. The tree is empty\n";
        }
    }
    void RemoveSubtree(Node* ptr) //post order traversal actually
    {
        if(ptr != NULL)
        {
            if(ptr->left != NULL)
            {
                RemoveSubtree(ptr->left);
            }
            if(ptr->right != NULL)
            {
                RemoveSubtree(ptr->right);
            }
            cout << "Deleting the node containing key " << ptr->key << endl;
            delete ptr;
        }
    }

public:
    BST()
    {
        root = NULL;
    }
    /*
    ~BST()
    {
        RemoveSubtree(root);
    }
    */
    Node* CreateLeaf(int key, double data) //add toprivate
    {
        Node* node_ptr = new Node;
        node_ptr->key = key; // first key is the key inside node, second key is the key being passed in
        node_ptr->data = data;
        node_ptr->left = NULL;
        node_ptr->right = NULL;

        return node_ptr;
    }

    int AddLeaf(int key, double data, int sorttime)
    {
        return AddLeafPrivate(key, root, data, sorttime);
    }
    void PrintInOrder()
    {
        PrintInOrderPrivate(root);
    }
    void PrintInOrderToFile(ofstream& OutputFile)
    {
        PrintInOrderToFilePrivate(root, OutputFile);
    }
    Node* ReturnNode(int key, double data) //helper function //add to private
    {
        ReturnNodePrivate(key, root, data);
    }
    int ReturnRootKey()
    {
        if(root != NULL)
        {
            return root->key;
        }
        else
        {
            //will need to adjust if we have negative numbers
        }
    }
    void PrintChildren(int key, double data)
    {
        Node* ptr = ReturnNode(key, data);

        if(ptr != NULL)
        {
            cout << "Parent Node = " << ptr->key << endl;
            cout << "NAME: " << ptr->data << endl;

            ptr->left == NULL ?
            cout << "Left Child = NULL\n":  //true
            cout << "Left Child = " << ptr->left->key << endl; //false
            cout << "NAME: " << ptr->data << endl;

            ptr->right == NULL ?
            cout << "Right Child = NULL\n":
            cout << "Right Child = " << ptr->right->key << endl;
            cout << "NAME: " << ptr->data << endl;
        }
        else
        {
            cout << "Key " << key << " is not in the tree\n";
        }
    }
    int FindSmallest()
    {
        FindSmallestPrivate(root);
    }
    void RemoveNode(int key)
    {
        RemoveNodePrivate(key, root, false);
    }


};


int SelectionSort(Array a[], int length)
{
    int sorttime = 0;
    
    int smallest;
    int walker;
    int minIndex;

    for(int curr = 0; curr < length - 1; curr++)
    {
        smallest = a[curr].key;

        for(walker = curr + 1; walker < length; walker++)
        {
            if(a[walker].key < smallest)
            {
                smallest = a[walker].key;
                minIndex = walker;
            }
            sorttime++;
        }
        a[minIndex].key = a[curr].key;
        a[curr].key = smallest;
	
    }
    
    return sorttime;
}



int InsertSort(Array a[], int length)
{
    int sorttime = 0;

    for(int curr = 1; curr < length ; curr++)
    {
        int hold = a[curr].key , walker = curr - 1;

        while(walker >= 0 && hold < a[walker].key)
        {
            a[walker + 1].key = a[walker].key; walker--;
        }
        sorttime++;
        a[walker+1].key = hold;
	
	
    }
    
    return sorttime;
}

/*
void BubbleSort(Array a[], int length)
{
    int last = a[length - 1];
    int curr = 0;
    int walker;
    bool sorted = false;

    while(curr <= last && !sorted)
    {
        walker = a[last];
        sorted = true;
        while(curr() > a[curr])
        {
            if(walker < )
            {
                sorted = false;
            //exchange list, walker, ...?
            }
        }

    }
}
*/

int BubbleSort(Array a[], int length)
{
    int sorttime = 0;
   
    int temp;
    for(int curr = 0; curr < length; curr++)
    {
        for(int walker = 0; walker < length; walker++)
        {
            if(a[curr].key < a[walker].key)
            {
                temp = a[curr].key;
                a[curr].key = a[walker].key;
                a[walker].key = temp;
            }
            sorttime++;
        }
    }
    return sorttime ;
}

/*
void QuickSort(int a[], int length)
{
    left = a[0];
    right = a[length - 1];

    if((right-left) > minsize)
    {

    }
}
*/


int QuickSort(Array a[], int left, int right)
{
    int sorttime = 0;
   
    int i = left;
    int j = right;
    int temp;
    int pivot = a[(left+right)/2].key;
    while(i <= j)
    {
        while(a[i].key < pivot)
	{
           i++;
	   sorttime++;
	}
        while(a[j].key > pivot)
	{
           j--;
	   sorttime++;	   
	}
        if(i <= j)
        {
            temp = a[i].key;
            a[i].key = a[j].key;
            a[j].key = temp;
            i++;
            j++;
        }
        sorttime++;
    }
    if(left < j)
    {
        sorttime += QuickSort(a, left, j);
    }
    if(i < right)
    {
        sorttime += QuickSort(a, i, right);
    }

    return sorttime;
}



void PrintArray(Array a[], int length)
{
    for(int i = 0; i < length; i++)
    {
        cout << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
	cout << endl;
    }
    cout << endl;
}

void PrintArrayToFile(Array a[], int length, ofstream& OutputFile)
{
    int count = 1;
    
    if(length == 100)
    {
       for(int i = 0; i < length; i++)
       {
       OutputFile << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
       OutputFile << endl;
       }
    OutputFile << endl;
    }
    
    if(length == 500)
    {
       for(int i = 0; i < length; i++)
       {
       OutputFile << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
       OutputFile << endl;
       }
    OutputFile << endl;
    }
    
    if(length == 1000)
    {
       for(int i = 0; i < length; i++)
       {
       OutputFile << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
       OutputFile << endl;
       }
    OutputFile << endl;
    }
    
}


int main()
{    
    const int small = 100;
    const int medium = 500;
    const int large = 1000;
    Array a[small];
    Array b[medium];
    Array c[large];
    
    
    
    srand(unsigned(time(0)));
    string file;
    string file2;
    string file3;
    
    for(int i = 0; i < small; i++)
    {
       a[i].key = (rand() % 5000) + 1;
       a[i].data = (rand() % 15000) + 1;
    }
       
    for(int i = 0; i < medium; i++)
    {
       b[i].key = (rand() % 5000) + 1;
       b[i].data = (rand() % 15000) + 1;
    }
       
    for(int i = 0; i < large; i++)
    {
       c[i].key = (rand() % 5000) + 1;
       c[i].data = (rand() % 15000) + 1;
    }
    
    
    
    //Objects for BSTs
    BST Small;
    BST Medium;
    BST Large;
    
    
    //Variables for clock storage
    double clock = 0;
    double clock2 = 0;
    double clock3 = 0;
    
    
    
    bool toFile = true;
    
    //for tree
    bool BST_MODE = false;
    bool small_tree = false;
    bool medium_tree = false;
    bool large_tree = false;
    //end
    
    //for sorts
    bool Selection_Sort = true;
    bool Insert_Sort = true;
    bool Bubble_Sort = true;
    bool Quick_Sort = false; // Bug -- (bigger than like 10) -> crash
    //end
    
    cout << endl;
    cout << "Before sorting" << endl;
    PrintArray(a, small);
    cout << endl;


    //BST CAN BE PRINTED TO FILE OR TO SCREEEN
    //NEED TIME FROM IT
    if(BST_MODE)
    {
        cout << endl << "Printing from tree" << endl << endl;
	if( small_tree == false && medium_tree == false && large_tree == false )
	{
	   cout << "No tree is selected" << endl;
	   cout << "Try checking boolean values for small/medium/large_tree 's" << endl;
	}
        if(small_tree)
        {
            for(int i = 0; i < small; i++)
            {
                clock += Small.AddLeaf((rand() % 5000) + 1, (rand() % 15000) + 1, 0);
            }
            cout << endl << "Small data set printed from tree" << endl;
            Small.PrintInOrder();
            cout << endl;
	    cout << endl << clock << endl << endl;
        }
        if(medium_tree)
        {
            for(int i = 0; i < medium; i++)
            {
                clock2 += Medium.AddLeaf((rand() % 5000) + 1, (rand() % 15000) + 1, 0);
            }
            cout << endl << "Medium data set printed from tree" << endl;
            Medium.PrintInOrder();
            cout << endl;
	    cout << endl << clock2 << endl << endl;
        }
        if(large_tree)
        {
            for(int i = 0; i < large; i++)
            {
                clock3 += Large.AddLeaf((rand() % 5000) + 1, (rand() % 15000) + 1, 0);
            }
            cout << endl << "Large data set printed from tree" << endl;
            Large.PrintInOrder();
            cout << endl;
	    cout << endl << clock3 << endl << endl;
        }
        if(toFile)
	{
	   ofstream OutputFile;
           OutputFile.open("BST.dat");
           file = "BST.dat";
           if(OutputFile.fail())
           {
               cout << "Failure to open file" << endl;
               exit(EXIT_FAILURE);
           }
           cout << "BST printed to file " << file << endl;
           OutputFile << "After sorting with a binary search tree" << endl;
           Small.PrintInOrderToFile(OutputFile);
	   cout << endl;
	}
    }
   cout << " START " << clock << endl << clock2 << endl <<  clock3 << endl;
   cout << "end" << endl;

    cout << endl;

    //List 100;


    
    
    if(Selection_Sort)
    {
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
       
        if(toFile)
        {
	    cout << endl << "-------Begining of Selection Sort-------" << endl << endl;
	        
	    clock = SelectionSort(copyA, small);
	    
	    clock2 = SelectionSort(copyB, medium);
	     
            clock3 = SelectionSort(copyC, large);
	    
	    cout << endl;
	    cout << clock;
	    cout << endl;
	    
	    cout << endl;
	    cout << clock2;
	    cout << endl;
	    
	    cout << endl;
	    cout << clock3;
	    cout << endl;
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("sels100.dat");
	    OutputFile2.open("sels500.dat");
	    OutputFile3.open("sels1000.dat");
            file = "sels100.dat";
	    file2 = "sels500.dat";
	    file3 = "sels1000.dat";
	    
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Selection Sort"  << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Selection Sort"  << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Selection Sort"  << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
        }
        else
        {
            cout << endl << "After sorting with Selection Sort" << endl;
	    cout << "WITH SMALL DATA SET" << endl << endl;
            SelectionSort(copyA, small);
            PrintArray(copyA, small);
        }
        cout << "START" << endl;
        cout << endl << endl << clock << endl << clock2 << endl << clock3 << endl;
	cout << "END OF SELECTION SORT" << endl;

    
    }

    if(Insert_Sort)
    {
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
        
        if(toFile)
        {
	    cout << endl << "--------Begining of Insert Sort--------" << endl << endl;
	    
	    clock = InsertSort(copyA, small);
	    
	    clock2 = InsertSort(copyB, medium);
	     
            clock3 = InsertSort(copyC, large);
	    
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("sins100.dat");
	    OutputFile2.open("sins500.dat");
	    OutputFile3.open("sins1000.dat");
            file = "sins100.dat";
	    file2 = "sins500.dat";
	    file3 = "sins1000.dat";
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Insert Sort"  << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Insert Sort"  << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Insert Sort"  << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
	    
        }
        else
        {
            cout << endl << "After sorting with Insert Sort" << endl;
            InsertSort(copyA, small);
            PrintArray(copyA, small);
        }
        
        cout << "START" << endl;
        cout << endl << endl << clock << endl << clock2 << endl << clock3 << endl;
	cout << "END OF second sort SORT" << endl;

    }

    
    if(Bubble_Sort)
    {
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
        
        if(toFile)
        {
	    cout << endl << "--------Begining of Bubble Sort --------" << endl << endl;
	    
	    clock = BubbleSort(copyA, small);
	    
	    clock2 = BubbleSort(copyB, medium);
	     
            clock3 = BubbleSort(copyC, large);
	    
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("bub100.dat");
	    OutputFile2.open("bub500.dat");
	    OutputFile3.open("bub1000.dat");
            file = "bub100.dat";
	    file2 = "bub500.dat";
	    file3 = "bub1000.dat";
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Bubble Sort"  << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Bubble Sort"  << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Bubble Sort"  << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
	    
        }
        else
        {
            cout << endl << "After sorting with Bubble Sort" << endl;
            BubbleSort(copyA, small);
            PrintArray(copyA, small);
        }
        cout << "START" << endl;
        cout << endl << endl << clock << endl << clock2 << endl << clock3 << endl;
	cout << "END OF last  SORT" << endl;
    }


    if(Quick_Sort)
    {
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
        
        if(toFile)
        {
	    cout << endl << "---------Begining of Quick Sort---------" << endl << endl;
	    
	    clock = QuickSort(copyA, 0, small);
	    
	    clock2 = QuickSort(copyB, 0, medium);
	     
            clock3 = QuickSort(copyC, 0, large);
	    
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("quick100.dat");
	    OutputFile2.open("quick500.dat");
	    OutputFile3.open("quick1000.dat");
            file = "quick100.dat";
	    file2 = "quick500.dat";
	    file3 = "quick1000.dat";
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Quick Sort"  << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Quick Sort"  << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Quick Sort"  << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
	    
        }
        else
        {
            cout << endl << "After sorting with Quick Sort" << endl;
            QuickSort(copyA, 0, 50);
            PrintArray(copyA, small);
        }


    }

    cout << "Big - O calculated value: " << endl; // ADD CALCULATION
    cout << "Selection sort time: " << endl; // ADD CALCULATION
    cout << "Straight insertion sort time: " << endl; // ADD CALCULATION
    cout << "Bubble sort time: " << endl; // ADD CALCULATION
    cout << "Quick sort time: " << endl; // ADD CALCULATION
    cout << "Binary search tree time: " << endl; // ADD CALCULATION

    
    
    return 0;
}
