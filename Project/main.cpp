/******************************************************************************

   Brandon Jones - N534H699
   Tyler Hull - P622S636

       PROJECT
   
   Description of problem:
      We wish to create arrays of sizes 100, 500, 1000, and use sorts to 
	 properly organize data based off the key correlating to the data
	    and compare to other data in the array to create an ascending
	       list of elements. to be printed to a file and/or screen.
		  utilizes the sort time for each sort, and calculated
		     calculations are on sheet of paper, or can be accessed
			from this program, under debug mode, and
			   printofile = false
	       
      
   
   


*/


#include <iostream>
#include <cstdlib>
//for clock access
#include <ctime>
//for file access
#include <fstream>

using namespace std;


//the structure we store our information in
//(IDEA WILL BE TO HAVE AN ARRAY OF STRUCTURES)
struct Array
{
   int key;
   float data;
};

//include file names
#include "bst.cpp"
#include "bubble.cpp"
#include "insertion.cpp"
#include "selection.cpp"
#include "quick.cpp"


//prints the array we wish, passing in the array itself and length
void PrintArray(Array a[], int length)
{
    for(int i = 0; i < length; i++)
    {
        cout << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
	cout << endl;
    }
    cout << endl;
}

//prints array to file we wish to, passing in the file and array/length
//checks data set size needed to output
void PrintArrayToFile(Array a[], int length, ofstream& OutputFile)
{
    
    if(length == 100)
    {
       for(int i = 0; i < length; i++)
       {
       OutputFile << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
       OutputFile << endl;
       }
    OutputFile << endl;
    }
    
    if(length == 500)
    {
       for(int i = 0; i < length; i++)
       {
       OutputFile << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
       OutputFile << endl;
       }
    OutputFile << endl;
    }
    
    if(length == 1000)
    {
       for(int i = 0; i < length; i++)
       {
       OutputFile << "Key: " << a[i].key << "    " << "Data: " << a[i].data;
       OutputFile << endl;
       }
    OutputFile << endl;
    }
    
}

//the start for the program (main)
int main()
{    
    // small data size (100 elements)
    const int small = 100;
    // medium data size (500 elements)
    const int medium = 500;
    // large data size (1000 elements)
    const int large = 1000;
    //declares our arrays of type struct
    //we have a small, medium and large array
    Array a[small];
    Array b[medium];
    Array c[large];
    
    
    //to access random numbers
    srand(unsigned(time(0)));
    //the use of file is to output the file name the output is printed to
    string file;
    string file2;
    string file3;
    
    //array to initalize our small data array
    for(int i = 0; i < small; i++)
    {
       a[i].key = (rand() % 5000) + 1;
       a[i].data = (rand() % 15000) + 1;
    }
       
    //array to initalize our medium data array   
    for(int i = 0; i < medium; i++)
    {
       b[i].key = (rand() % 5000) + 1;
       b[i].data = (rand() % 15000) + 1;
    }
       
    //array to initalize our large data array   
    for(int i = 0; i < large; i++)
    {
       c[i].key = (rand() % 5000) + 1;
       c[i].data = (rand() % 15000) + 1;
    }
    
    //allows access to debuging features
    bool debug_mode = false;
    
    //Objects for BSTs
    BST Small;
    BST Medium;
    BST Large;
    
    
    //Variables for iteration storage
    double clock = 0;
    double clock2 = 0;
    double clock3 = 0;
    
    //print to file? yes-true no-false
    //Note: false will print to screen
    bool toFile = true;
    
    //for tree
    //access booleans of where to print
    bool BST_MODE = true;
    bool small_tree = true;
    bool medium_tree = true;
    bool large_tree = true;
    //end
    
    //for sorts
    //access booleans of where to print 
    bool Selection_Sort = true;
    bool Insert_Sort = true;
    bool Bubble_Sort = true;
    bool Quick_Sort = true; // Bug -- (bigger than like 10) -> crash
    //end
    
    //prints a small array as a test before sorting
    //ckan moddify for any data size
    if(debug_mode)
    {
       cout << endl;
       cout << "Before sorting" << endl;
       PrintArray(a, small);
       cout << endl;
    }

    //BST CAN BE PRINTED TO FILE OR TO SCREEEN
    //NEED TIME FROM IT
    //stores time taken
    int value = 0;
    int value2 = 0;
    int value3 = 0;
    
    //Binary search tree mode
    if(BST_MODE)
    {
        
        cout << endl << "Printing from tree" << endl << endl;
	if( small_tree == false && medium_tree == false && large_tree == false )
	{
	   cout << "No tree is selected" << endl;
	   cout << "Try checking boolean values for small/medium/large_tree 's" << endl;
	}
	//prints small tree using binary tree
        if(small_tree)
        {
            for(int i = 0; i < small; i++)
            {
                clock += Small.AddLeaf(a[i].key, a[i].data);
            }
            value = Small.PrintInOrderNEW();
            if(debug_mode)
	    {
	       cout << endl << "Small data set printed from tree" << endl;
	       Small.PrintInOrder();
	       cout << endl;
	       cout << endl << clock << endl << endl;
	       cout << endl << "VALUE SMALL" << endl << value;
	    }
	    
        }
        //prints medium tree using binary tree
        if(medium_tree)
        {
            for(int i = 0; i < medium; i++)
            {
                clock2 += Medium.AddLeaf(b[i].key, b[i].data);
            }
            value2 = Medium.PrintInOrderNEW();
            if(debug_mode)
	    {
	       cout << endl << "Medium data set printed from tree" << endl;
	       Medium.PrintInOrder();
	       cout << endl;
	       cout << endl << clock2 << endl << endl;
	       cout << endl << "VALUE MEDIUM" << endl << value2;
	    }
        }
        //prints large tree using binary tree
        if(large_tree)
        {
            for(int i = 0; i < large; i++)
            {
                clock3 += Large.AddLeaf(c[i].key, c[i].data);
            }
            value3 = Large.PrintInOrderNEW();
            if(debug_mode)
	    {
	       cout << endl << "Large data set printed from tree" << endl;
	       Large.PrintInOrder();
	       cout << endl;
	       cout << endl << clock3 << endl << endl;
	       cout << endl << "VALUE LARGE" << endl << value3;
	    }
        }
	
	//prints BST to file
        if(toFile)
	{
	   ofstream OutputFile;
	   ofstream OutputFile2;
	   ofstream OutputFile3;
           OutputFile.open("BST100.dat");
	   OutputFile2.open("BST500.dat");
	   OutputFile3.open("BST1000.dat");
           file = "BST100.dat";
	   file2 = "BST500.dat";
	   file3 = "BST1000.dat";
	   
	   
	   //checks file open status
           if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
           {
	       //debug with testing individual files
               cout << "Failure to open file" << endl;
               exit(EXIT_FAILURE);
           }
           
           cout << endl << endl;
	   
           cout << "BST printed to file " << file << endl;
           OutputFile << endl << "After sorting with a binary search tree" << endl;
	   OutputFile << endl << "It took " << clock+value << " iterations to sort and print" << endl;
	   OutputFile << endl << "Data size: " << small << endl << endl;
           Small.PrintInOrderToFile(OutputFile);
	   cout << endl;
	   
	   cout << "BST printed to file " << file2 << endl;
           OutputFile2 << endl << "After sorting with a binary search tree" << endl;
	   OutputFile2 << endl << "It took " << clock2+value2 << " iterations to sort and print" << endl;
	   OutputFile2 << endl << "Data size: " << medium << endl << endl;
           Medium.PrintInOrderToFile(OutputFile2);
	   cout << endl;
	   
	   cout << "BST printed to file " << file3 << endl;
           OutputFile3 << endl << "After sorting with a binary search tree" << endl;
	   OutputFile3 << endl << "It took " << clock3+value3 << " iterations to sort and print" << endl;
	   OutputFile3 << endl << "Data size: " << large << endl << endl;
           Large.PrintInOrderToFile(OutputFile3);
	   cout << endl;
	}
    }
    //used to track interation time
    if(debug_mode)
    {
       cout << " START BST " << endl << clock << endl << clock2 << endl <<  clock3 << endl;
       cout << "end" << endl;
    }

    cout << endl;

    //List 100;


    
    //sorts with selection sort
    if(Selection_Sort)
    {
        //uses a copy of array to be used to do a sort (allows for unique sorting)
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
	//initializes the new "copy" array to the initial array.
	//does for all array sizes
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
       //sorts the data set, prints to file, and tracks iteration time.
        if(toFile)
        {
	    cout << endl << "-------Begining of Selection Sort-------" << endl << endl;
	        
	    clock = SelectionSort(copyA, small);
	    
	    clock2 = SelectionSort(copyB, medium);
	     
            clock3 = SelectionSort(copyC, large);
	    
	    if(debug_mode)
	    {
	       cout << endl;
	       cout << clock;
	       cout << endl;
	       
	       cout << endl;
	       cout << clock2;
	       cout << endl;
	       
	       cout << endl;
	       cout << clock3;
	       cout << endl;
	    }
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("sels100.dat");
	    OutputFile2.open("sels500.dat");
	    OutputFile3.open("sels1000.dat");
            file = "sels100.dat";
	    file2 = "sels500.dat";
	    file3 = "sels1000.dat";
	    
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Selection Sort"  << endl;
	    OutputFile << endl << "It took " << clock << " iterations to sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Selection Sort"  << endl;
	    OutputFile2 << endl << "It took " << clock2 << " iterations to sort" << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Selection Sort"  << endl;
	    OutputFile3 << endl << "It took " << clock3 << " iterations to sort" << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
        }
        else
        {
            cout << endl << "After sorting with Selection Sort" << endl;
	    cout << "WITH SMALL DATA SET" << endl << endl;
            SelectionSort(copyA, small);
            PrintArray(copyA, small);
        }
        if(debug_mode)
	{
	   cout << "START" << endl;
	   cout << endl << endl << clock << endl << clock2 << endl << clock3 << endl;
	   cout << "END OF SELECTION SORT" << endl;
	}

    
    }

    if(Insert_Sort)
    {
        //initalizes arrays
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
	//initializes the new "copy" array to the initial array.
	//does for all array sizes
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
        
        //sorts the data set, prints to file, and tracks iteration time.
        if(toFile)
        {
	    cout << endl << "--------Begining of Insert Sort--------" << endl << endl;
	    
	    clock = InsertSort(copyA, small);
	    
	    clock2 = InsertSort(copyB, medium);
	     
            clock3 = InsertSort(copyC, large);
	    
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("sins100.dat");
	    OutputFile2.open("sins500.dat");
	    OutputFile3.open("sins1000.dat");
            file = "sins100.dat";
	    file2 = "sins500.dat";
	    file3 = "sins1000.dat";
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Insert Sort"  << endl;
	    OutputFile << endl << "It took " << clock << " iterations to sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Insert Sort"  << endl;
	    OutputFile2 << endl << "It took " << clock2 << " iterations to sort" << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Insert Sort"  << endl;
	    OutputFile3 << endl << "It took " << clock3 << " iterations to sort" << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
	    
        }
        else
        {
            cout << endl << "After sorting with Insert Sort" << endl;
            InsertSort(copyA, small);
            PrintArray(copyA, small);
        }
        
        if(debug_mode)
	{
	   cout << "START INSERT SORT" << endl;
	   cout << endl << endl << clock << endl << clock2 << endl << clock3 << endl;
	   cout << "END OF INSERT sort SORT" << endl;
	}

    }

    
    if(Bubble_Sort)
    {
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
	//initializes the new "copy" array to the initial array.
	//does for all array sizes
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
        
        //sorts the data set, prints to file, and tracks iteration time.
        if(toFile)
        {
	    cout << endl << "--------Begining of Bubble Sort --------" << endl << endl;
	    
	    clock = BubbleSort(copyA, small);
	    
	    clock2 = BubbleSort(copyB, medium);
	     
            clock3 = BubbleSort(copyC, large);
	    
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("bub100.dat");
	    OutputFile2.open("bub500.dat");
	    OutputFile3.open("bub1000.dat");
            file = "bub100.dat";
	    file2 = "bub500.dat";
	    file3 = "bub1000.dat";
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Bubble Sort"  << endl;
	    OutputFile << endl << "It took " << clock << " iterations to sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Bubble Sort"  << endl;
	    OutputFile2 << endl << "It took " << clock2 << " iterations to sort" << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Bubble Sort"  << endl;
	    OutputFile3 << endl << "It took " << clock3 << " iterations to sort" << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
	    
        }
        else
        {
            cout << endl << "After sorting with Bubble Sort" << endl;
            BubbleSort(copyA, small);
            PrintArray(copyA, small);
        }
        
        if(debug_mode)
	{
	   cout << "START BUB SORT" << endl;
	   cout << endl << endl << clock << endl << clock2 << endl << clock3 << endl;
	   cout << "END OF BUB  SORT" << endl;
	}
	
    }


    if(Quick_Sort)
    {
        Array copyA[small];
	Array copyB[medium];
	Array copyC[large];
	
	//initializes the new "copy" array to the initial array.
	//does for all array sizes
        for(int i = 0; i < small; i++)
        {
           copyA[i].key = a[i].key;
           copyA[i].data = a[i].data;
        }
        for(int i = 0; i < medium; i++)
        {
           copyB[i].key = b[i].key;
           copyB[i].data = b[i].data;
        }
        for(int i = 0; i < large; i++)
        {
           copyC[i].key = c[i].key;
           copyC[i].data = c[i].data;
        }
        
        //sorts the data set, prints to file, and tracks iteration time.
        if(toFile)
        {
	    cout << endl << "---------Begining of Quick Sort---------" << endl << endl;
	    
	    clock = QuickSort(copyA, 0, small - 1);
	    
	    clock2 = QuickSort(copyB, 0, medium - 1);
	     
            clock3 = QuickSort(copyC, 0, large - 1);
	    
	    
            ofstream OutputFile;
	    ofstream OutputFile2;
	    ofstream OutputFile3;
            OutputFile.open("quick100.dat");
	    OutputFile2.open("quick500.dat");
	    OutputFile3.open("quick1000.dat");
            file = "quick100.dat";
	    file2 = "quick500.dat";
	    file3 = "quick1000.dat";
	    
	    //checks file open status
            if(OutputFile.fail() || OutputFile2.fail() || OutputFile3.fail())
            {
	        //debug with testing individual files
                cout << "Failure to open file" << endl;
                exit(EXIT_FAILURE);
            }
            
            //Prints to a file
            cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file << endl << endl;
            OutputFile << endl << "After sorting with Quick Sort"  << endl;
	    OutputFile << endl << "It took " << clock << " iterations to sort" << endl;
            OutputFile << endl << "There are " << small << " elements being sorted\n\n";
            PrintArrayToFile(copyA, small, OutputFile);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file2 << endl << endl;
            OutputFile2 << endl << "After sorting with Quick Sort"  << endl;
	    OutputFile2 << endl << "It took " << clock2 << " iterations to sort" << endl;
            OutputFile2 << endl << "There are " << medium << " elements being sorted\n\n";
            PrintArrayToFile(copyB, medium, OutputFile2);
	    
	    //Prints to a file
	    cout << endl << "Printing to file..." << endl;
            cout << "Successfully printed to file " << file3 << endl << endl;
            OutputFile3 << endl << "After sorting with Quick Sort"  << endl;
	    OutputFile3 << endl << "It took " << clock3 << " iterations to sort" << endl;
            OutputFile3 << endl << "There are " << large << " elements being sorted\n\n";
            PrintArrayToFile(copyC, large, OutputFile3);
	    
	    //close files
	    OutputFile.close();
	    OutputFile2.close();
	    OutputFile3.close();
	    
        }
        else
        {
            cout << endl << "After sorting with Quick Sort" << endl;
            QuickSort(copyA, 0, 50);
            PrintArray(copyA, small);
        }
        
        if(debug_mode)
	{
	   cout << "AFTER QUICK SORT TIME" << endl;
	   cout << endl << clock << endl << clock2 << endl << clock3 << endl << endl;
	}


    }
    
    //prints out the calculated values of operation time (IN BIG O FASHION)
    cout << "Big - O calculated value: " << endl; // ADD CALCULATION
    cout << "                                          100         500        1000 " << endl;
    cout << "______________________________________________________________________" << endl;
    cout << "Selection sort time: O(n^2)=              10000       250000     1e+06" << endl; // ADD CALCULATION
    cout << "Straight insertion sort time: O(n^2)=     10000       250000     1e+06" << endl; // ADD CALCULATION
    cout << "Bubble sort time: O(n^2)=                 10000       250000     1e+06" << endl; // ADD CALCULATION
    cout << "Quick sort time: O(n log n)=              200         1349.5     3000 " << endl; // ADD CALCULATION
    cout << "Binary search tree time: O(n^2=           10000       250000     1e+06" << endl; // ADD CALCULATION

    //if debuged, printed time to print from BST, used in debugging BST TOTAL print time
    //(ADDED TO CLOCK TIME IN BST)
    if(debug_mode)
    {
       cout << endl << endl << endl << value << endl << value2 << endl << value3 << endl;
    }
    
    return 0;
}
